#include <gtest/gtest.h>
#include "SmallVector.h"

namespace Rw
{

	TEST(SmallVectorTest, IsSmallUpToCapacity) {
		SmallVector<int> stack{};
		for (int i = 0; i < SmallVector<int>::SmallCapacity; ++i) {
			stack.push(i);
		}
		EXPECT_TRUE(stack.isSmall());
		stack.push(-1);
		EXPECT_FALSE(stack.isSmall());
	}

	TEST(SmallVectorTest, PushingElementsIsReflectedInSize) {
		SmallVector<int> stack{};
		EXPECT_EQ(stack.size(), 0);
		for (int i = 0; i < SmallVector<int>::SmallCapacity; ++i) {
			stack.push(i);
			EXPECT_EQ(stack.size(), i + 1);
		}

		stack.push(-1);
		EXPECT_EQ(stack.size(), SmallVector<int>::SmallCapacity + 1);
	}

	TEST(SmallVectorTest, TopGetsTheLastElement) {
		SmallVector<int> stack{};
		for (int i = 0; i < SmallVector<int>::SmallCapacity; ++i) {
			stack.push(i);
			EXPECT_EQ(stack.top(), i);
		}

		stack.push(-1);
		EXPECT_EQ(stack.top(), -1);
	}

	TEST(SmallVectorTest, PopRemovesTheLastElement) {
		SmallVector<int> stack{};
		for (int i = 0; i < SmallVector<int>::SmallCapacity; ++i) {
			stack.push(i);
			EXPECT_EQ(stack.top(), i);
		}

		stack.push(-1);
		int last = stack.pop();
		EXPECT_EQ(last, -1);
		EXPECT_NE(stack.top(), last);

		for (int i = stack.size(); i > 0; --i) {
			last = stack.pop();
			EXPECT_EQ(last, stack.size());
		}
	}

	TEST(SmallVectorTest, CopiesAnotherVector) {
		SmallVector<int> vec{};
		vec.push(1);
		vec.push(2);
		vec.push(3);

		SmallVector<int> copy{ vec };
		EXPECT_EQ(vec.size(), copy.size());
		EXPECT_EQ(vec[0], copy[0]);
		EXPECT_EQ(vec[1], copy[1]);
		EXPECT_EQ(vec[2], copy[2]);
	}

}