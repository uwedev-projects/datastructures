﻿find_package (GTest REQUIRED HINTS ${CMAKE_CURRENT_BINARY_DIR}/../../../generators)

set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

add_executable (QuadTreeTest "testmain.cpp")

target_compile_features (QuadTreeTest PRIVATE cxx_std_23)
target_compile_definitions (QuadTreeTest PRIVATE RW_TEST_EXE)

target_include_directories(QuadTreeTest
    PRIVATE 
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/../inc>
)

target_link_libraries (QuadTreeTest PRIVATE gtest::gtest)

target_sources (QuadTreeTest 
    PRIVATE 
        ${CMAKE_CURRENT_SOURCE_DIR}/../src/SmallVector.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/SmallVectorTest.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/../src/FreeList.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/FreeListTest.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/../src/QuadTree.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/QuadTreeTest.cpp
)

add_test (NAME QuadTreeTest COMMAND QuadTreeTest)

include(GoogleTest)
gtest_discover_tests(QuadTreeTest)
