#include <gtest/gtest.h>
#include "FreeList.h"

namespace Rw
{

	TEST(FreeListTest, InsertElementWithoutFreeSlot) {
		FreeList<i64> flist{};
		const i32 i0 = flist.insert(1);
		const i32 i1 = flist.insert(2);
		const i32 i2 = flist.insert(3);

		EXPECT_EQ(flist.size(), 3);
		EXPECT_EQ(flist[i0], 1);
		EXPECT_EQ(flist[i1], 2);
		EXPECT_EQ(flist[i2], 3);
	}


	TEST(FreeListTest, InsertElementWithFreeSlot) {
		FreeList<i64> flist{};
		const i32 i0 = flist.insert(1);
		const i32 i1 = flist.insert(2);
		const i32 i2 = flist.insert(3);

		flist.erase(i1);
		const i32 i1Reused = flist.insert(4);

		EXPECT_EQ(flist.size(), 3);
		EXPECT_EQ(flist[i1Reused], 4);
		EXPECT_EQ(i1Reused, i1);
	}

	TEST(FreeListTest, EraseElementInTheMiddle) {
		FreeList<i64> flist{};
		const i32 i0 = flist.insert(1);
		const i32 i1 = flist.insert(2);
		const i32 i2 = flist.insert(3);

		flist.erase(i1);

		EXPECT_EQ(flist.size(), 3);
		EXPECT_EQ(flist[i0], 1);
		EXPECT_EQ(flist[i2], 3);
	}

	TEST(FreeListTest, IteratorSkipsFreeElements) {
		FreeList<i64> flist{};
		const i32 i0 = flist.insert(1);
		const i32 i1 = flist.insert(2);
		const i32 i2 = flist.insert(3);

		flist.erase(i1);

		auto itList = flist.begin();
		EXPECT_EQ(*itList, 1);

		++itList;
		EXPECT_EQ(*itList, 3);
	}

}