#include <gtest/gtest.h>
#include "QuadTree.h"

#include <set>

namespace Rw
{
	Rect getQuadExtent() {
		return { -100, -100, 100, 100 };
	}

	TEST(QuadTreeTest, TestIntersection) {
		QuadTree quad{ getQuadExtent() };
		quad.insert({ 20, -30, 30, -20 }, 1);
		quad.insert({ -90, -90, -80, -80 }, 2);
		quad.insert({ -40, 15, -25, 30 }, 3);
		quad.insert({ -50, 60, -30, 80 }, 4);

		EXPECT_TRUE(quad.testIntersection(Rect{ -30, 10, -20, 20 }));
		EXPECT_TRUE(quad.testIntersection(Rect{ -60, 60, -49, 80 }));
		EXPECT_FALSE(quad.testIntersection(Rect{ -60, 60, -50, 80 }));
		EXPECT_FALSE(quad.testIntersection(Rect{ -20, -50, -10, -40 }));
	}

	TEST(QuadTreeTest, QueryIntersectionExact) {
		QuadTree quad{ getQuadExtent() };
		quad.insert({ 10, 10, 20, 20 }, 1);
		quad.insert({ 30, 10, 40, 20 }, 2);
		quad.insert({ 50, 10, 60, 20 }, 3);

		SmallVector<i32> result{ quad.queryIntersection(Rect{10, 10, 60, 20}) };
		EXPECT_EQ(result.size(), 3);
		
		std::set<i32> expected{ 1, 2, 3 };

		while (!result.isEmpty()) {
			const i32 id = result.top();
			result.pop();

			EXPECT_TRUE(expected.count(id) == 1);
			expected.erase(id);
		}

		EXPECT_TRUE(expected.empty());
	}
}