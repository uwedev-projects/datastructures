#include "World.h"

#include <random>
#include <cmath>

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

namespace Rw
{

	namespace WorldConstants {
		constexpr u32 NumAgents = 1000;
		constexpr f32 AgentMinSize = 10.0f;
		constexpr f32 AgentMaxSize = 20.0f;
		constexpr f32 MinCoord = -1000.0f;
		constexpr f32 MaxCoord =  1000.0f;

		const Rect Map{ static_cast<i32>(MinCoord), static_cast<i32>(MinCoord), static_cast<i32>(MaxCoord), static_cast<i32>(MaxCoord) };
	}

	Agent generateRandomAgent(std::mt19937& rng) {
		std::uniform_real_distribution<f32> distCoords{ WorldConstants::MinCoord, 0.0f };
		std::uniform_real_distribution<f32> distSize{ WorldConstants::AgentMinSize, WorldConstants::AgentMaxSize };
		std::uniform_real_distribution<f32> distDirection{ 0.0f, glm::two_pi<f32>() };
		std::uniform_real_distribution<f32> distSpeed{ 20.0f, 70.0f };

		const f32 width = distSize(rng);
		const f32 height = distSize(rng);

		f32 left = distCoords(rng);
		f32 top = distCoords(rng);

		if (left + width > WorldConstants::MaxCoord)
			left = WorldConstants::MaxCoord - width;

		if (top + height > WorldConstants::MaxCoord)
			top = WorldConstants::MaxCoord - height;

		const f32 angle = distDirection(rng);
		const glm::vec2 direction{ glm::cos(angle), glm::sin(angle) };
		const glm::vec2 velocity{ direction * 500.0f };
		return Agent{ glm::vec2{left, top}, glm::vec2{left + width, top + height}, velocity };
	}

	[[ nodiscard ]] inline Rect agentToRect(const Agent& agent) noexcept {
		return { std::lround(agent.topLeft.x), std::lround(agent.topLeft.y), std::lround(agent.botRight.x), std::lround(agent.botRight.y) };
	}

	inline void rectToAgent(const Rect& rect, Agent& agent) noexcept {
		agent.topLeft.x = rect.left;
		agent.topLeft.y = rect.top;
		agent.botRight.x = rect.right;
		agent.botRight.y = rect.bottom;
	}

	[[ nodiscard ]] glm::vec2 calcCollisionNormal(const Agent& first, const Agent& second) {
		const glm::vec2 centerFirst { (first.topLeft  + first.botRight) * 0.5f };
		const glm::vec2 centerSecond{ (second.topLeft + second.botRight) * 0.5f };

		const f32 deltaX = centerFirst.x - centerSecond.x;
		const f32 deltaY = centerFirst.y - centerSecond.y;

		const f32 overlapX = (first.botRight.x - first.topLeft.x) * 0.5f + (second.botRight.x - second.topLeft.x) * 0.5f - std::abs(deltaX);
		const f32 overlapY = (first.botRight.y - first.topLeft.y) * 0.5f + (second.botRight.y - second.topLeft.y) * 0.5f - std::abs(deltaY);

		glm::vec2 result{0.0f, 0.0f};
		if (overlapX < overlapY)
			result.x = deltaX < 0 ? -1.0f : 1.0f;
		else
			result.y = deltaY < 0 ? -1.0f : 1.0f;

		return result;
	}

	[[ nodiscard ]] glm::vec2 calcShortestSeparation(const Agent& first, const Agent& second) {
		const glm::vec2 centerFirst { (first.topLeft + first.botRight) * 0.5f };
		const glm::vec2 centerSecond{ (second.topLeft + second.botRight) * 0.5f };

		const f32 deltaX = centerFirst.x - centerSecond.x;
		const f32 deltaY = centerFirst.y - centerSecond.y;

		const f32 overlapX = (first.botRight.x - first.topLeft.x) * 0.5f + (second.botRight.x - second.topLeft.x) * 0.5f - std::abs(deltaX);
		const f32 overlapY = (first.botRight.y - first.topLeft.y) * 0.5f + (second.botRight.y - second.topLeft.y) * 0.5f - std::abs(deltaY);

		glm::vec2 result{ 
			deltaX < 0.0f ? -overlapX : overlapX, 
			deltaY < 0.0f ? -overlapY : overlapY
		};

		if (overlapX < overlapY && overlapX > 0.0f) {
			result.y = 0.0f;
		}
		else if (overlapY > 0.0f) {
			result.x = 0.0f;
		}

		return result;
	}

	[[ nodiscard ]] glm::vec2 reflect(const glm::vec2& velocity, const glm::vec2& normal) {
		return velocity - 2.0f * glm::dot(velocity, normal) * normal;
	}

	World::World() :
		tree{ WorldConstants::Map }, agents{}
	{
		agents.reserve(WorldConstants::NumAgents);
		tree.onCollision([this](i32 idFirst, i32 idSecond) {
			Agent& first = agents[idFirst - 1];
			Agent& second = agents[idSecond - 1];

			const glm::vec2 shortestSeparation{ calcShortestSeparation(first, second) };
			const glm::vec2 collisionNormal{
				std::clamp(shortestSeparation.x, -1.0f, 1.0f),
				std::clamp(shortestSeparation.y, -1.0f, 1.0f),
			};

			const glm::vec2 relativeVelocity{ first.velocity - second.velocity };
			const f32 penetrationSpeed = glm::dot(relativeVelocity, collisionNormal);

			//if (penetrationSpeed < 0.0f) {
				//tree.erase(agentToRect(first), idFirst);
				tree.erase(agentToRect(first), idFirst);
				first.topLeft  += shortestSeparation;
				first.botRight += shortestSeparation;
				//const Rect rect{ agentToRect(first) };
				//tree.insert(rect, idFirst);
				//rectToAgent(rect, first);
			//}

			Rect rectFirst{ agentToRect(first) };
			Rect rectSecond{ agentToRect(second) };
			assert(!isIntersecting(rectFirst, rectSecond));

			//const glm::vec2 penetration{ collisionNormal * penetrationSpeed };

			const glm::vec2 firstReflect  = reflect(first.velocity, collisionNormal);
			const glm::vec2 secondReflect = reflect(second.velocity, -collisionNormal);

			first.velocity  = firstReflect;
			second.velocity = secondReflect;
		});
	}

	void World::createAgents() {
		std::random_device rd{};
		std::mt19937 rng{ rd() };

		while (agents.size() < WorldConstants::NumAgents) {
			const Agent agent{ generateRandomAgent(rng) };
			const Rect rect{ agentToRect(agent) };
			if (!tree.testIntersection(rect)) {
				tree.insert(rect, static_cast<i32>(agents.size() + 1));
				agents.push_back(agent);
			}
		}
	}

	void World::update(f32 dt) {
		i32 id = 1;
		for (Agent& agent : agents) {
			tree.erase(agentToRect(agent), id);

			moveAgent(agent, dt);
			const Rect rect{ agentToRect(agent) };
			tree.insert(rect, id++);
			rectToAgent(rect, agent);
		}

		tree.checkCollisions();
		tree.cleanupEmptyLeaves();
	}

	void World::moveAgent(Agent& agent, f32 dt) {
		agent.topLeft += agent.velocity * dt;
		agent.botRight += agent.velocity * dt;

		if (agent.topLeft.x < WorldConstants::MinCoord) {
			const f32 width = agent.botRight.x - agent.topLeft.x;
			agent.topLeft.x  = WorldConstants::MinCoord;
			agent.botRight.x = agent.topLeft.x + width;
			agent.velocity.x = -agent.velocity.x;
		}

		if (agent.botRight.x > WorldConstants::MaxCoord) {
			const f32 width = agent.botRight.x - agent.topLeft.x;
			agent.botRight.x = WorldConstants::MaxCoord;
			agent.topLeft.x  = agent.botRight.x - width;
			agent.velocity.x = -agent.velocity.x;
		}

		if (agent.topLeft.y < WorldConstants::MinCoord) {
			const f32 height = agent.botRight.y - agent.topLeft.y;
			agent.topLeft.y  = WorldConstants::MinCoord;
			agent.botRight.y = agent.topLeft.y + height;
			agent.velocity.y = -agent.velocity.y;
		}

		if (agent.botRight.y > WorldConstants::MaxCoord) {
			const f32 height = agent.botRight.y - agent.topLeft.y;
			agent.botRight.y = WorldConstants::MaxCoord;
			agent.topLeft.y = agent.botRight.y - height;
			agent.velocity.y = -agent.velocity.y;
		}
	}
}