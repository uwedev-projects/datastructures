#include "QuadTreeRenderer.h"
#include "FileSystem.h"
#include "SmallVector.h"

#include <glbinding/gl/gl.h>
#include <glm/gtc/matrix_transform.hpp>
#include <cmath>
#include <array>

namespace Rw
{

	namespace ElementData {
		const std::array colors{
			glm::vec4{ 0.7f, 0.2f, 0.2f, 1.0f },
			glm::vec4{ 0.3f, 0.8f, 0.2f, 1.0f },
			glm::vec4{ 0.4f, 0.2f, 1.0f, 1.0f },
			glm::vec4{ 0.7f, 0.1f, 0.7f, 1.0f }
		};

		glm::vec4 getElementColor(i32 id) {
			return glm::vec4{ 0.2f, 0.6f, 0.8f, 1.0f };
		}
	}

	namespace QuadData {
		glm::vec4 getColorByDepth(i32 depth) {
			glm::vec4 color{ 1.0f, 1.0f, 1.0f, 1.0f };
			if (depth > 0) {
				color.r /= depth;
				color.b /= depth;
			}
			return color;
		}
	}

	QuadTree::QuadTreeRenderer::QuadTreeRenderer(const QuadTree* tree) :
		projection{ glm::ortho(-2000.0f, 2000.0f, 2000.0f, -2000.0f, -1.0f, 1.0f) },
		shader{}, quadModel{}, elementModel{}, quadInstances{}, elementInstances{}, tree{ tree }, quadInstanceVBO{}, elementInstanceVBO{}
	{
		const Fs::Path vertexShaderSourceFile{ std::filesystem::current_path()
			.append("Shader").append("QuadVert.glsl")
		};

		const Fs::Path fragmentShaderSourceFile{ std::filesystem::current_path()
			.append("Shader").append("QuadFrag.glsl")
		};

		const Err rc = shader
			.addShader(ShaderType::Vertex, vertexShaderSourceFile)
			.addShader(ShaderType::Fragment, fragmentShaderSourceFile)
			.build();

		quadModel.vertices.reserve(4);
		quadModel.vertices.emplace_back(-0.5f,  0.5f);
		quadModel.vertices.emplace_back( 0.5f,  0.5f);
		quadModel.vertices.emplace_back( 0.5f, -0.5f);
		quadModel.vertices.emplace_back(-0.5f, -0.5f);

		quadModel.indices = { 0, 1, 1, 2, 2, 3, 3, 0 };

		gl::glGenVertexArrays(1, &quadModel.vertexArrayId);
		gl::glGenBuffers(1, &quadModel.vertexBufferId);
		gl::glGenBuffers(1, &quadModel.indexBufferId);

		gl::glBindVertexArray(quadModel.vertexArrayId);
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, quadModel.vertexBufferId);

		gl::glBufferData(gl::GL_ARRAY_BUFFER, quadModel.vertices.size() * sizeof(glm::vec2), quadModel.vertices.data(), gl::GL_STATIC_DRAW);

		gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, quadModel.indexBufferId);
		gl::glBufferData(gl::GL_ELEMENT_ARRAY_BUFFER, quadModel.indices.size() * sizeof(u32), quadModel.indices.data(), gl::GL_STATIC_DRAW);

		gl::glVertexAttribPointer(0, 2, gl::GL_FLOAT, gl::GL_FALSE, sizeof(glm::vec2), nullptr);
		gl::glEnableVertexAttribArray(0);

		prepareQuadInstances();

		gl::glBindVertexArray(0);
		gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, 0);
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);

		elementModel.vertices.reserve(4);
		elementModel.vertices.emplace_back(-0.5f, 0.5f);
		elementModel.vertices.emplace_back(0.5f, 0.5f);
		elementModel.vertices.emplace_back(0.5f, -0.5f);
		elementModel.vertices.emplace_back(-0.5f, -0.5f);

		elementModel.indices = { 
			0, 1, 2, 
			0, 2, 3 
		};

		gl::glGenVertexArrays(1, &elementModel.vertexArrayId);
		gl::glGenBuffers(1, &elementModel.vertexBufferId);
		gl::glGenBuffers(1, &elementModel.indexBufferId);

		gl::glBindVertexArray(elementModel.vertexArrayId);
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, elementModel.vertexBufferId);

		gl::glBufferData(gl::GL_ARRAY_BUFFER, elementModel.vertices.size() * sizeof(glm::vec2), elementModel.vertices.data(), gl::GL_STATIC_DRAW);

		gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, elementModel.indexBufferId);
		gl::glBufferData(gl::GL_ELEMENT_ARRAY_BUFFER, elementModel.indices.size() * sizeof(u32), elementModel.indices.data(), gl::GL_STATIC_DRAW);

		gl::glVertexAttribPointer(0, 2, gl::GL_FLOAT, gl::GL_FALSE, sizeof(glm::vec2), nullptr);
		gl::glEnableVertexAttribArray(0);

		prepareElementInstances();

		gl::glBindVertexArray(0);
		gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, 0);
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
	}

	QuadTree::QuadTreeRenderer::~QuadTreeRenderer()
	{
		gl::glDeleteVertexArrays(1, &quadModel.vertexArrayId);
		gl::glDeleteBuffers(1, &quadModel.vertexBufferId);
		gl::glDeleteBuffers(1, &quadModel.indexBufferId);
		gl::glDeleteBuffers(1, &quadInstanceVBO);

		gl::glDeleteVertexArrays(1, &elementModel.vertexArrayId);
		gl::glDeleteBuffers(1, &elementModel.vertexBufferId);
		gl::glDeleteBuffers(1, &elementModel.indexBufferId);
		gl::glDeleteBuffers(1, &elementInstanceVBO);
	}

	void QuadTree::QuadTreeRenderer::prepareQuadInstances() {
		quadInstances.reserve(MaxQuadInstances);

		SmallVector<QuadTree::NodeData> toProcess{};
		toProcess.push({ tree->rootExtent, 0, 0 });

		while (!toProcess.isEmpty()) {
			QuadTree::NodeData current{ toProcess.pop() };
			const QuadTree::Node& node = tree->nodes[current.index];
			if (node.count == BranchNode) {
				const i32 quadCenterX = (current.rect.left + current.rect.right) / 2;
				const i32 quadCenterY = (current.rect.top + current.rect.bottom) / 2;

				const Rect topLeft { current.rect.left, current.rect.top, quadCenterX, quadCenterY };
				const Rect topRight{ quadCenterX, current.rect.top, current.rect.right, quadCenterY };
				const Rect botLeft { current.rect.left, quadCenterY, quadCenterX, current.rect.bottom };
				const Rect botRight{ quadCenterX, quadCenterY, current.rect.right, current.rect.bottom };

				toProcess.push({ topLeft,  node.firstChild + 0, current.depth + 1 });
				toProcess.push({ topRight, node.firstChild + 1, current.depth + 1 });
				toProcess.push({ botLeft,  node.firstChild + 2, current.depth + 1 });
				toProcess.push({ botRight, node.firstChild + 3, current.depth + 1 });

				quadInstances.emplace_back(
					glm::vec2{ current.rect.left, current.rect.top },
					glm::vec2{ current.rect.right, current.rect.bottom },
					QuadData::getColorByDepth (current.depth));
			}
		}

		gl::glGenBuffers(1, &quadInstanceVBO);
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, quadInstanceVBO);
		gl::glBufferData(gl::GL_ARRAY_BUFFER, sizeof(QuadInstance) * quadInstances.size(), quadInstances.data(), gl::GL_STATIC_DRAW);

		gl::glVertexAttribPointer(1, 2, gl::GL_FLOAT, gl::GL_FALSE, sizeof(QuadInstance), reinterpret_cast<void*>(offsetof(QuadInstance, topLeft)));
		gl::glEnableVertexAttribArray(1);
		gl::glVertexAttribDivisor(1, 1);

		gl::glVertexAttribPointer(2, 2, gl::GL_FLOAT, gl::GL_FALSE, sizeof(QuadInstance), reinterpret_cast<void*>(offsetof(QuadInstance, bottomRight)));
		gl::glEnableVertexAttribArray(2);
		gl::glVertexAttribDivisor(2, 1);

		gl::glVertexAttribPointer(3, 4, gl::GL_FLOAT, gl::GL_FALSE, sizeof(QuadInstance), reinterpret_cast<void*>(offsetof(QuadInstance, color)));
		gl::glEnableVertexAttribArray(3);
		gl::glVertexAttribDivisor(3, 1);
	}

	void QuadTree::QuadTreeRenderer::prepareElementInstances() {
		elementInstances.reserve(tree->elements.size());

		for (const QuadTree::Element& element : tree->elements) {
			elementInstances.emplace_back(
				glm::vec2{ element.rect.left, element.rect.top },
				glm::vec2{ element.rect.right, element.rect.bottom },
				ElementData::colors[element.id & 3]
			);
		}

		gl::glGenBuffers(1, &elementInstanceVBO);
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, elementInstanceVBO);
		gl::glBufferData(gl::GL_ARRAY_BUFFER, sizeof(QuadInstance) * elementInstances.size(), elementInstances.data(), gl::GL_STATIC_DRAW);

		gl::glVertexAttribPointer(1, 2, gl::GL_FLOAT, gl::GL_FALSE, sizeof(QuadInstance), reinterpret_cast<void*>(offsetof(QuadInstance, topLeft)));
		gl::glEnableVertexAttribArray(1);
		gl::glVertexAttribDivisor(1, 1);

		gl::glVertexAttribPointer(2, 2, gl::GL_FLOAT, gl::GL_FALSE, sizeof(QuadInstance), reinterpret_cast<void*>(offsetof(QuadInstance, bottomRight)));
		gl::glEnableVertexAttribArray(2);
		gl::glVertexAttribDivisor(2, 1);

		gl::glVertexAttribPointer(3, 4, gl::GL_FLOAT, gl::GL_FALSE, sizeof(QuadInstance), reinterpret_cast<void*>(offsetof(QuadInstance, color)));
		gl::glEnableVertexAttribArray(3);
		gl::glVertexAttribDivisor(3, 1);
	}

	void QuadTree::QuadTreeRenderer::render(f32 width, f32 height) {
		if (tree->nodes.size() != quadInstances.size())
			updateQuadData();

		updateElementData();

		shader.use();
		shader.setMat4("proj", projection);

		gl::glBindVertexArray(quadModel.vertexArrayId);
		gl::glDrawElementsInstanced(gl::GL_LINES, quadModel.indices.size(), gl::GL_UNSIGNED_INT, 0, quadInstances.size());

		gl::glBindVertexArray(elementModel.vertexArrayId);
		gl::glDrawElementsInstanced(gl::GL_TRIANGLES, elementModel.indices.size(), gl::GL_UNSIGNED_INT, 0, elementInstances.size());

		gl::glBindVertexArray(0);
	}

	void QuadTree::QuadTreeRenderer::updateQuadData() {
		quadInstances.clear();

		SmallVector<QuadTree::NodeData> toProcess{};
		toProcess.push({ tree->rootExtent, 0, 0 });

		while (!toProcess.isEmpty()) {
			QuadTree::NodeData current{ toProcess.top() };
			toProcess.pop();

			const QuadTree::Node& node = tree->nodes[current.index];
			if (node.count == BranchNode) {
				const i32 quadCenterX = (current.rect.left + current.rect.right) / 2;
				const i32 quadCenterY = (current.rect.top + current.rect.bottom) / 2;

				const Rect topLeft{ current.rect.left, current.rect.top, quadCenterX, quadCenterY };
				const Rect topRight{ quadCenterX, current.rect.top, current.rect.right, quadCenterY };
				const Rect botLeft{ current.rect.left, quadCenterY, quadCenterX, current.rect.bottom };
				const Rect botRight{ quadCenterX, quadCenterY, current.rect.right, current.rect.bottom };

				toProcess.push({ topLeft,  node.firstChild + 0, current.depth + 1 });
				toProcess.push({ topRight, node.firstChild + 1, current.depth + 1 });
				toProcess.push({ botLeft,  node.firstChild + 2, current.depth + 1 });
				toProcess.push({ botRight, node.firstChild + 3, current.depth + 1 });
			}

			quadInstances.emplace_back(
				glm::vec2{ current.rect.left, current.rect.top },
				glm::vec2{ current.rect.right, current.rect.bottom },
				QuadData::getColorByDepth(current.depth));
		}

		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, quadInstanceVBO);
		gl::glBufferData(gl::GL_ARRAY_BUFFER, sizeof(QuadInstance) * quadInstances.size(), quadInstances.data(), gl::GL_STATIC_DRAW);
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
	}

	void QuadTree::QuadTreeRenderer::updateElementData() {
		elementInstances.clear();
		elementInstances.reserve(tree->elements.size());

		for (const QuadTree::Element& element : tree->elements) {
			elementInstances.emplace_back(
				glm::vec2{ element.rect.left, element.rect.top },
				glm::vec2{ element.rect.right, element.rect.bottom },
				ElementData::colors[element.id & 3]
				//ElementData::colors[0]
			);
		}

		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, elementInstanceVBO);
		gl::glBufferData(gl::GL_ARRAY_BUFFER, sizeof(QuadInstance) * elementInstances.size(), elementInstances.data(), gl::GL_STATIC_DRAW);
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
	}

}