#include "Window.h"

#include <glbinding/glbinding.h>
#include <glbinding/gl/gl.h>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <cassert>
#include "QuadTree.h"

namespace Rw
{
	Window* mainWindow = nullptr;

	void handleWindowResized(GLFWwindow* window, int width, int height) noexcept {
		mainWindow->onResize(window, width, height);
	}

	Window::Window() noexcept
		: wnd {}, width{ 800 }, height{ 800 }, backgroundColor{ 0.0f, 0.15f, 0.15f, 1.0f }
	{
		using namespace std::placeholders;
		glfwInit();

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		wnd = glfwCreateWindow(width, height, "QuadTreeDemo", nullptr, nullptr);
		glfwMakeContextCurrent(wnd);

		glbinding::initialize(glfwGetProcAddress);

		assert(mainWindow == nullptr);
		mainWindow = this;
		glfwSetFramebufferSizeCallback(wnd, handleWindowResized);
	}

	Window::~Window()
	{
		glfwDestroyWindow(wnd);
		glfwTerminate();
	}

	void Window::onResize(GLFWwindow* window, i32 width, i32 height) noexcept {
		this->width = width;
		this->height = height;
	}

	void Window::clear() noexcept {
		glfwGetFramebufferSize(wnd, &width, &height);

		gl::glViewport(0, 0, width, height);
		gl::glClearColor(
			backgroundColor.x * backgroundColor.w,
			backgroundColor.y * backgroundColor.w,
			backgroundColor.z * backgroundColor.w,
			backgroundColor.w
		);
		gl::glClear(gl::GL_COLOR_BUFFER_BIT);
	}

	void Window::render(const QuadTree& tree) noexcept {
		clear();
		tree.render(static_cast<f32>(width), static_cast<f32>(height));
		glfwSwapBuffers(wnd);
		glfwPollEvents();
	}

	bool Window::isClosed() const noexcept {
		return glfwWindowShouldClose(wnd);
	}
}