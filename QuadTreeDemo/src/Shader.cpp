#include "Shader.h"
#include "FileSystem.h"

#include <glbinding/gl/gl.h>
#include <glbinding/glbinding.h>
#include <glm/gtc/type_ptr.hpp>

#include <sstream>
#include <fstream>
#include <utility>
#include <cassert>

namespace Rw
{
	using namespace gl;

	gl::GLenum shaderTypeToGLType(ShaderType type) noexcept {
		gl::GLenum glType = gl::GL_VERTEX_SHADER;
		switch (type) {
		case ShaderType::Undefined:
		case ShaderType::Vertex:
			glType = gl::GL_VERTEX_SHADER;
			break;
		case ShaderType::Fragment:
			glType = gl::GL_FRAGMENT_SHADER;
			break;
		case ShaderType::Geometry:
			glType = gl::GL_GEOMETRY_SHADER;
			break;
		}

		return glType;
	}

	std::string getShaderErrorMsg(u32 shaderId) {
		constexpr std::size_t bufferSize = 512;
		char glErrBuffer[bufferSize];
		i32 bytesWritten = 0;
		gl::glGetShaderInfoLog(shaderId, bufferSize, &bytesWritten, glErrBuffer);
		return bytesWritten > 0 ? std::string{ glErrBuffer, glErrBuffer + bytesWritten + 1 } : std::string{};
	}

	std::string getProgramErrorMsg(u32 shaderId) {
		constexpr std::size_t bufferSize = 512;
		char glErrBuffer[bufferSize];
		i32 bytesWritten = 0;
		gl::glGetProgramInfoLog(shaderId, bufferSize, &bytesWritten, glErrBuffer);
		return bytesWritten > 0 ? std::string{ glErrBuffer, glErrBuffer + bytesWritten + 1 } : std::string{};
	}

	Shader::Shader(std::string_view sourceFile, std::ostream& logStream, ShaderType type)
		: source{}, sourceFile{ sourceFile }, logStream{ &logStream }, type{ type }, id{}
	{
		std::ifstream inputStream{ sourceFile.data() };
		std::stringstream sourceBuffer{};

		if (inputStream.good())
		{
			sourceBuffer << inputStream.rdbuf();
			source = sourceBuffer.str();
		}
	}

	Shader::Shader(Shader&& other) noexcept 
		: source{std::move(other.source)}, sourceFile{ std::move(other.sourceFile) }, logStream{ other.logStream }, type { other.type }, id {other.id}
	{
		other.type = ShaderType::Undefined;
	}

	Shader& Shader::operator=(Shader&& rhs) noexcept {
		source = std::move(rhs.source);
		sourceFile = std::move(rhs.sourceFile);
		logStream = rhs.logStream;
		type = rhs.type;
		id = rhs.id;
		rhs.type = ShaderType::Undefined;
		return *this;
	}

	Shader::~Shader() {
		if (type != ShaderType::Undefined)
			gl::glDeleteShader(id);
	}

	Err Shader::compile() {
		Err rc = Err::OK;

		const gl::GLenum glType = shaderTypeToGLType(type);
		id = gl::glCreateShader(glType);
		const char* sourceCStr = source.c_str();

		gl::glShaderSource(id, 1, &sourceCStr, nullptr);
		gl::glCompileShader(id);

		int success = 0;
		gl::glGetShaderiv(id, GL_COMPILE_STATUS, &success);
		if (logStream) {
			if (success) {
				*logStream 
					<< "Shader source file " << sourceFile 
					<< " successfully compiled." << "\n";
			}
			else {
				const std::string glErrMsg{ getShaderErrorMsg(id) };

				*logStream
					<< "Shader compilation failed for file: " << sourceFile << ".\n"
					<< "Error message: " << glErrMsg << "\n";
				rc = Err::Error;
			}
		}

		return rc;
	}

	ShaderProgram::ShaderProgram() : 
		shaders{}, 
		logStream{ "glsl.log" },
		id{}
	{
		shaders.resize(NumShaderTypes);
	}

	ShaderProgram::~ShaderProgram() {
		gl::glDeleteProgram(id);
	}

	ShaderProgram& ShaderProgram::addShader(ShaderType type, Fs::Path sourceFile) {
		assert(type != ShaderType::Undefined);
		const std::size_t index = static_cast<std::size_t>(type) - 1;
		shaders[index] = Shader{ sourceFile.generic_string(), logStream, type};
		return *this;
	}

	Err ShaderProgram::build() {
		if (shaders.empty())
			return Err::Error;

		Err rc = Err::OK;

		for (int i = 0; rc == Err::OK && i < shaders.size(); ++i) {
			Shader& shader = shaders[i];
			if (shader.isInitialized()) {
				rc = shader.compile();
			}
		}

		id = gl::glCreateProgram();

		for (const Shader& shader : shaders) {
			if (shader.isInitialized()) {
				gl::glAttachShader(id, shader.getId());
			}
		}

		gl::glLinkProgram(id);
		int success = 0;
		gl::glGetProgramiv(id, GL_LINK_STATUS, &success);

		if (success) {
			logStream
				<< "Successfully built shader program #" << std::to_string(id) << "\n";
		}
		else {
			const std::string glErrMsg{ getProgramErrorMsg(id) };

			logStream
				<< "Shader build failed for shader program #" << std::to_string(id) << ".\n"
				<< "Error message: " << glErrMsg << "\n";
			rc = Err::Error;
		}

		logStream.flush();
		shaders.clear();

		return rc;
	}

	Err ShaderProgram::validate() {
		Err rc = Err::OK;

		int success{};
		gl::glGetProgramiv(id, GL_VALIDATE_STATUS, &success);
		if (success) {
			logStream << "Validation succeeded for shader program #" << std::to_string(id) << ".\n";
		}
		else {
			const std::string glErrMsg{ getProgramErrorMsg(id) };
			logStream << "Validation failed for shader program #" << std::to_string(id) << ".\n";
			logStream << "Error message: " << glErrMsg << "\n";
			rc = Err::Error;
		}

		logStream.flush();

		return rc;
	}

	void ShaderProgram::use() const noexcept {
		gl::glUseProgram(id);
	}

	ShaderProgram& ShaderProgram::setSampler(std::string_view name, int value) noexcept {
		gl::glUniform1i(glGetUniformLocation(id, name.data()), value);
		return *this;
	}

	ShaderProgram& ShaderProgram::setFloat(std::string_view name, f32 f) noexcept {
		gl::glUniform1f(glGetUniformLocation(id, name.data()), f);
		return *this;
	}

	ShaderProgram& ShaderProgram::setVec4(std::string_view name, float x, float y, float z, float w) noexcept {
		gl::glUniform4f(glGetUniformLocation(id, name.data()), x, y, z, w);
		return *this;
	}

	ShaderProgram& ShaderProgram::setVec3(std::string_view name, float x, float y, float z) noexcept {
		gl::glUniform3f(glGetUniformLocation(id, name.data()), x, y, z);
		return *this;
	}

	ShaderProgram& ShaderProgram::setVec2(std::string_view name, float x, float y) noexcept {
		gl::glUniform2f(glGetUniformLocation(id, name.data()), x, y);
		return *this;
	}

	ShaderProgram& ShaderProgram::setMat4(std::string_view name, const glm::mat4& mat) noexcept {
		gl::glUniformMatrix4fv(glGetUniformLocation(id, name.data()), 1, GL_FALSE, glm::value_ptr(mat));
		return *this;
	}

}