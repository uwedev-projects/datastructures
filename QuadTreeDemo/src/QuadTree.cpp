#include "QuadTree.h"

#ifndef RW_TEST_EXE
	#include "QuadTreeRenderer.h"
#endif

#include <utility>
#include <cassert>
#include <fstream>
#include <format>

namespace Rw
{
	QuadTree::QuadTree(const Rect& extent) :
		QuadTree(extent, 4)
	{

	}

	QuadTree::QuadTree(const Rect& extent, i32 elementLimit) :
		nodes{ Node{} }, 
		elements{}, 
		elementNodes{}, 
		handler{},
		rootExtent{ extent }, 
#ifndef RW_TEST_EXE
		renderer{ std::make_unique<QuadTreeRenderer>(this) },
#endif
		firstFree{ NullIndex }, 
		elementLimit{ elementLimit }
	{

	}

	QuadTree::QuadTree(const QuadTree& other) :
		nodes{ other.nodes },
		elements{ other.elements },
		elementNodes{ other.elementNodes },
		handler{ other.handler },
		rootExtent{ other.rootExtent },
#ifndef RW_TEST_EXE
		renderer{ std::make_unique<QuadTreeRenderer>(this) },
#endif
		firstFree{ other.firstFree },
		elementLimit{ elementLimit }
	{

	}

	QuadTree& QuadTree::operator=(const QuadTree& other)
	{
		nodes = other.nodes;
		elements = other.elements;
		elementNodes = other.elementNodes;
		handler = other.handler;
		rootExtent = other.rootExtent;
#ifndef RW_TEST_EXE
		renderer = std::make_unique<QuadTreeRenderer>(this);
#endif
		firstFree = other.firstFree;
		elementLimit = other.elementLimit;

		return *this;
	}

	QuadTree::QuadTree(QuadTree&& other) noexcept :
		nodes{ std::move(other.nodes) },
		elements{ std::move(other.elements) },
		elementNodes{ std::move(other.elementNodes) },
		handler{ std::move(other.handler) },
		rootExtent{ std::move(other.rootExtent) },
#ifndef RW_TEST_EXE
		renderer{ std::move(other.renderer) },
#endif
		firstFree{ other.firstFree },
		elementLimit{ other.elementLimit }
	{

	}

	QuadTree& QuadTree::operator=(QuadTree&& other) noexcept {
		nodes = std::move(other.nodes);
		elements = std::move(other.elements);
		elementNodes = std::move(other.elementNodes);
		handler = std::move(other.handler);
		rootExtent = std::move(other.rootExtent);
#ifndef RW_TEST_EXE
		renderer = std::move(other.renderer);
#endif
		firstFree = other.firstFree;
		elementLimit = other.elementLimit;

		return *this;
	}

	QuadTree::~QuadTree() = default;

#ifndef RW_TEST_EXE
	void QuadTree::render(f32 width, f32 height) const {
		renderer->render(width, height);
	}
#endif

	void QuadTree::insert(const Rect& rect, i32 id) {
		SmallVector<NodeData> toProcess{};
		toProcess.push({ rootExtent, 0, 0 });
		i32 elementIndex = NullIndex;

		while (!toProcess.isEmpty()) {
			NodeData current{ toProcess.top() };
			Node& node{ nodes[current.index] };
			toProcess.pop();

			if (node.count == BranchNode) {
				const i32 quadCenterX = (current.rect.left + current.rect.right) >> 1;
				const i32 quadCenterY = (current.rect.top + current.rect.bottom) >> 1;

				if (rect.top <= quadCenterY) {
					// Top left quadrant
					if (rect.left <= quadCenterX) {
						const Rect quadrant{ current.rect.left, current.rect.top, quadCenterX, quadCenterY };
						toProcess.push(NodeData{ quadrant, node.firstChild + 0, current.depth + 1 });
					}
					// Top right quadrant
					if (rect.right > quadCenterX) {
						const Rect quadrant{ quadCenterX, current.rect.top, current.rect.right, quadCenterY };
						toProcess.push(NodeData{ quadrant, node.firstChild + 1, current.depth + 1 });
					}
				}

				if (rect.bottom > quadCenterY) {
					// Bottom left quadrant
					if (rect.left <= quadCenterX) {
						const Rect quadrant{ current.rect.left, quadCenterY, quadCenterX, current.rect.bottom };
						toProcess.push(NodeData{ quadrant, node.firstChild + 2, current.depth + 1 });
					}
					// Bottom right quadrant
					if (rect.right > quadCenterX) {
						const Rect quadrant{ quadCenterX, quadCenterY, current.rect.right, current.rect.bottom };
						toProcess.push(NodeData{ quadrant, node.firstChild + 3, current.depth + 1 });
					}
				}
			}
			else {
				// Leaf 
				++node.count;

				if (elementIndex == NullIndex) {
					elementIndex = elements.insert({ rect, id });
				}

				const i32 nextElement = node.firstChild;
				node.firstChild = elementNodes.insert({ nextElement, elementIndex });

				if (node.count > elementLimit && current.depth < MaxDepth) {
					splitLeaf(current);
				}
			}
		}
	}

	bool QuadTree::erase(const Rect& rect, i32 id) {
		SmallVector<NodeData> toProcess{};
		toProcess.push({ rootExtent, 0, 0 });

		i32 elementToDelete = NullIndex;

		while (!toProcess.isEmpty()) {
			NodeData current{ toProcess.top() };
			toProcess.pop();
			Node& node = nodes[current.index];

			if (node.count == BranchNode) {
				const i32 quadCenterX = (current.rect.left + current.rect.right) >> 1;
				const i32 quadCenterY = (current.rect.top + current.rect.bottom) >> 1;

				if (rect.top <= quadCenterY) {
					// Top left quadrant
					if (rect.left <= quadCenterX) {
						const Rect quadrant{ current.rect.left, current.rect.top, quadCenterX, quadCenterY };
						toProcess.push({ quadrant, node.firstChild + 0, current.depth + 1 });
					}
					// Top right quadrant
					if (rect.right > quadCenterX) {
						const Rect quadrant{ quadCenterX, current.rect.top, current.rect.right, quadCenterY };
						toProcess.push({ quadrant, node.firstChild + 1, current.depth + 1 });
					}
				}

				if (rect.bottom > quadCenterY) {
					// Bottom left quadrant
					if (rect.left <= quadCenterX) {
						const Rect quadrant{ current.rect.left, quadCenterY, quadCenterX, current.rect.bottom };
						toProcess.push({ quadrant, node.firstChild + 2, current.depth + 1 });
					}
					// Bottom right quadrant
					if (rect.right > quadCenterX) {
						const Rect quadrant{ quadCenterX, quadCenterY, current.rect.right, current.rect.bottom };
						toProcess.push({ quadrant, node.firstChild + 3, current.depth + 1 });
					}
				}
			}
			else if (node.firstChild != NullIndex) {

				bool done = false;
				i32 currentElement = node.firstChild;
				ElementNode* prev = nullptr;
				while (currentElement != NullIndex && !done) {
					ElementNode& elementNode = elementNodes[currentElement];
					if (elements[elementNode.element].id == id && rect == elements[elementNode.element].rect) {
						elementToDelete = elementNode.element;

						if (prev != nullptr) {
							prev->next = elementNode.next;
						}
						else {
							node.firstChild = elementNode.next;
						}

						elementNodes.erase(currentElement);
						done = true;
					}
					else {
						prev = &elementNode;
						currentElement = elementNode.next;
					}
				}

				if (done) --node.count;
			}
		}

		if (elementToDelete != NullIndex) 
			elements.erase(elementToDelete);

		return elementToDelete != NullIndex;
	}

	void QuadTree::splitLeaf(QuadTree::NodeData& leafData) {
		const i32 newNodeIndex = createOrRecycleNodes();
		Node& node = nodes[leafData.index];
		i32 currentElement = node.firstChild;

		assert(node.count > elementLimit);

		while (currentElement != NullIndex) {
			ElementNode* elementNode = &elementNodes[currentElement];
			const Element& element = elements[elementNode->element];

			const i32 quadCenterX = (leafData.rect.left + leafData.rect.right) >> 1;
			const i32 quadCenterY = (leafData.rect.top + leafData.rect.bottom) >> 1;

			SmallVector<Node*> children{};

			if (element.rect.top <= quadCenterY) {
				// Top left quadrant
				if (element.rect.left <= quadCenterX) {
					children.push(&nodes[newNodeIndex + 0]);
				}
				// Top right quadrant
				if (element.rect.right > quadCenterX) {
					children.push(&nodes[newNodeIndex + 1]);
				}
			}

			if (element.rect.bottom > quadCenterY) {
				// Bottom left quadrant
				if (element.rect.left <= quadCenterX) {
					children.push(&nodes[newNodeIndex + 2]);
				}
				// Bottom right quadrant
				if (element.rect.right > quadCenterX) {
					children.push(&nodes[newNodeIndex + 3]);
				}
			}

			const i32 nextElement = elementNode->next;

			for (int i = 0; !children.isEmpty(); ++i) {
				Node* quadrant = children.top();
				children.pop();

				if (i != 0) {
					currentElement = elementNodes.insert(ElementNode{ NullIndex, elementNode->element });
					elementNode = &elementNodes[currentElement];
				}

				elementNode->next = quadrant->firstChild;
				quadrant->firstChild = currentElement;
				quadrant->count++;
			}

			currentElement = nextElement;
		}

		node.count = BranchNode;
		node.firstChild = newNodeIndex;
		
	}

	i32 QuadTree::createOrRecycleNodes() {
		i32 firstNodeIndex = nodes.size();
		if (firstFree == NullIndex) {
			nodes.emplace_back();
			nodes.emplace_back();
			nodes.emplace_back();
			nodes.emplace_back();
		}
		else {
			firstNodeIndex = firstFree;
			firstFree = nodes[firstFree].firstChild;
			nodes[firstNodeIndex].count = 0;
			nodes[firstNodeIndex].firstChild = NullIndex;
		}

		return firstNodeIndex;
	}

	void QuadTree::cleanupEmptyLeaves() {
		SmallVector<i32> toProcess{};
		if (nodes[0].count == BranchNode)
			toProcess.push(0);

		while (!toProcess.isEmpty()) {
			const i32 nodeIndex = toProcess.top();
			toProcess.pop();

			Node& node = nodes[nodeIndex];
			i32 numEmptyLeaves = 0;

			for (i32 i = 0; i < 4; ++i) {
				const i32 childIndex = node.firstChild + i;
				const Node& child = nodes[childIndex];
				if (child.count == 0)
					++numEmptyLeaves;
				else if (child.count == BranchNode)
					toProcess.push(childIndex);
			}

			if (numEmptyLeaves == 4) {
				nodes[node.firstChild].firstChild = firstFree;
				nodes[node.firstChild].count = FreeNode;
				firstFree = node.firstChild;

				node.count = 0;
				node.firstChild = NullIndex;
			}
		}

	}

	bool QuadTree::testIntersection(const Rect& rect) const {
		bool intersection = false;
		SmallVector<NodeData> toProcess{};
		toProcess.push({ rootExtent, 0, 0 });

		while (!toProcess.isEmpty()) {
			NodeData current{ toProcess.top() };
			const Node& node{ nodes[current.index] };
			toProcess.pop();

			if (node.count == BranchNode) {
				const i32 quadCenterX = (current.rect.left + current.rect.right) >> 1;
				const i32 quadCenterY = (current.rect.top + current.rect.bottom) >> 1;

				if (rect.top <= quadCenterY) {
					// Top left quadrant
					if (rect.left <= quadCenterX) {
						const Rect quadrant{ current.rect.left, current.rect.top, quadCenterX, quadCenterY };
						toProcess.push(NodeData{ quadrant, node.firstChild + 0, current.depth + 1 });
					}
					// Top right quadrant
					if (rect.right > quadCenterX) {
						const Rect quadrant{ quadCenterX, current.rect.top, current.rect.right, quadCenterY };
						toProcess.push(NodeData{ quadrant, node.firstChild + 1, current.depth + 1 });
					}
				}

				if (rect.bottom > quadCenterY) {
					// Bottom left quadrant
					if (rect.left <= quadCenterX) {
						const Rect quadrant{ current.rect.left, quadCenterY, quadCenterX, current.rect.bottom };
						toProcess.push(NodeData{ quadrant, node.firstChild + 2, current.depth + 1 });
					}
					// Bottom right quadrant
					if (rect.right > quadCenterX) {
						const Rect quadrant{ quadCenterX, quadCenterY, current.rect.right, current.rect.bottom };
						toProcess.push(NodeData{ quadrant, node.firstChild + 3, current.depth + 1 });
					}
				}
			}
			else {
				i32 elementNode = node.firstChild;
				while (elementNode != NullIndex && !intersection) {
					const ElementNode& currentElement = elementNodes[elementNode];
					const Element& element = elements[currentElement.element];
					intersection = isIntersecting(rect, element.rect);
					elementNode = currentElement.next;
				}
			}
		}

		return intersection;
	}

	SmallVector<i32> QuadTree::queryIntersection(const Rect& rect, i32 id) const {
		SmallVector<i32> intersectingElements{};

		SmallVector<NodeData> toProcess{};
		toProcess.push({ rootExtent, 0, 0 });

		while (!toProcess.isEmpty()) {
			NodeData current{ toProcess.top() };
			const Node& node{ nodes[current.index] };
			toProcess.pop();

			if (node.count == BranchNode) {
				const i32 quadCenterX = (current.rect.left + current.rect.right) >> 1;
				const i32 quadCenterY = (current.rect.top + current.rect.bottom) >> 1;

				if (rect.top <= quadCenterY) {
					// Top left quadrant
					if (rect.left <= quadCenterX) {
						const Rect quadrant{ current.rect.left, current.rect.top, quadCenterX, quadCenterY };
						toProcess.push(NodeData{ quadrant, node.firstChild + 0, current.depth + 1 });
					}
					// Top right quadrant
					if (rect.right > quadCenterX) {
						const Rect quadrant{ quadCenterX, current.rect.top, current.rect.right, quadCenterY };
						toProcess.push(NodeData{ quadrant, node.firstChild + 1, current.depth + 1 });
					}
				}

				if (rect.bottom > quadCenterY) {
					// Bottom left quadrant
					if (rect.left <= quadCenterX) {
						const Rect quadrant{ current.rect.left, quadCenterY, quadCenterX, current.rect.bottom };
						toProcess.push(NodeData{ quadrant, node.firstChild + 2, current.depth + 1 });
					}
					// Bottom right quadrant
					if (rect.right > quadCenterX) {
						const Rect quadrant{ quadCenterX, quadCenterY, current.rect.right, current.rect.bottom };
						toProcess.push(NodeData{ quadrant, node.firstChild + 3, current.depth + 1 });
					}
				}
			}
			else {
				i32 elementNode = node.firstChild;
				while (elementNode != NullIndex) {
					const ElementNode& currentElement = elementNodes[elementNode];
					const Element& element = elements[currentElement.element];

					if (id != element.id && isIntersecting(rect, element.rect))
						intersectingElements.push(element.id);

					elementNode = currentElement.next;
				}
			}
		}

		return intersectingElements;
	}

	SmallVector<i32> QuadTree::gatherLeaves() const {
		SmallVector<i32> leaves{};

		if (nodes.size() == 1) {
			leaves.push(0);
		}
		else {
			SmallVector<i32> toProcess{};
			toProcess.push(0);

			while (!toProcess.isEmpty()) {
				const i32 nodeIndex = toProcess.top();
				toProcess.pop();

				const Node& node = nodes[nodeIndex];

				for (i32 i = 0; i < 4; ++i) {
					const i32 childIndex = node.firstChild + i;
					const Node& child = nodes[childIndex];
					if (child.count == BranchNode) {
						toProcess.push(childIndex);
					}
					else {
						leaves.push(childIndex);
					}
				}
			}
		}

		return leaves;
	}

	void QuadTree::onCollision(const CollisionHandler& colHandler) {
		handler = colHandler;
	}

	void QuadTree::checkCollisions() const {
		SmallVector<i32> leaves{ gatherLeaves() };
		std::vector<bool> traversed{};
		traversed.resize(elements.size(), false);
		std::vector<SmallVector<i32>> collisionList{};
		collisionList.resize(elements.size() + 1);

		while (!leaves.isEmpty()) {
			const i32 leafIndex = leaves.top();
			leaves.pop();

			const Node& leaf = nodes[leafIndex];
			i32 currentElement = leaf.firstChild;
			while (currentElement != NullIndex) {
				const ElementNode& elementNode = elementNodes[currentElement];
				if (!traversed[elementNode.element]) {
					const Element& element = elements[elementNode.element];
					SmallVector<i32> intersections{ queryIntersection(element.rect, element.id) };
					addCollisions(element.id, intersections, collisionList);
					traversed[elementNode.element] = true;
				}
				currentElement = elementNode.next;
			}
		}

		for (i32 i = 0; i < collisionList.size(); ++i) {
			SmallVector<i32>& collisions = collisionList[i];
			if (!collisions.isEmpty()) {
				const i32 elementId = collisions.top();
				collisions.pop();
				handler(i, elementId);
			}
		}
	}

	void QuadTree::addCollisions(i32 elementId, SmallVector<i32>& intersections, std::vector<SmallVector<i32>>& collisionList) const {
		while (!intersections.isEmpty()) {
			const i32 collidingElementId = intersections.top();
			intersections.pop();

			if (!collisionList[collidingElementId].contains(elementId) && !collisionList[elementId].contains(collidingElementId)) {
				collisionList[elementId].push(collidingElementId);
			}
		}
	}
}

