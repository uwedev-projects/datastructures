#include "FileSystem.h"
#include "Common.h"

#include "Windows.h"
#include <cstdio>
#include <cassert>
#include <format>

namespace Rw
{

	namespace Fs
	{
		FileHandle::FileHandle(std::string_view filePath, std::string_view mode)
			: file{}, path { filePath }
		{
			file = std::fopen(path.c_str(), mode.data());
		}

		FileHandle::FileHandle(Path filePath, std::string_view mode)
			: file{}, path{ filePath.generic_string() }
		{
			file = std::fopen(path.c_str(), mode.data());
		}

		std::size_t FileHandle::write(const u8* data, std::size_t size, std::size_t count) {
			return std::fwrite(data, size, count, file);
		}

		std::size_t FileHandle::write(std::string_view str) {
			return std::fwrite(str.data(), sizeof(str[0]), str.size(), file);
		}

		void FileHandle::read(u8* data, std::size_t size, std::size_t count) {
			std::fread(data, size, count, file);
		}

		std::vector<char> FileHandle::read() {
			std::vector<char> buffer{};
			const i32 fileSize = getSize();

			if (fileSize < 0) {
				return buffer;
			}

			const u32 bufferSize = static_cast<u32>(fileSize);
			buffer.resize(bufferSize, '\0');
			const std::size_t countRead = std::fread(buffer.data(), bufferSize, 1, file);

			return buffer;
		}

		void FileHandle::flush() const noexcept {
			std::fflush(file);
		}

		i32 FileHandle::getSize() const noexcept {
			if (std::fseek(file, 0, SEEK_SET) != 0) {
				return -1;
			}

			if (std::fseek(file, 0, SEEK_END) != 0) {
				return -1;
			}

			const i32 size = std::ftell(file);
			if (size < 0) {
				return -1;
			}
			if (std::fseek(file, 0, SEEK_SET) != 0) {
				return -1;
			}

			return size;
		}

		i32 FileHandle::getCursorPos() const noexcept {
			const i32 pos = std::ftell(file);
			return pos > 0 ? pos : 0;
		}

		FileHandle::~FileHandle() {
			if (file) {
				std::fclose(file);
			}
		}
	}

}