﻿#include "Common.h"
#include "World.h"
#include "Window.h"
#include "QuadTree.h"

#include <chrono>
#include <thread>

using namespace Rw;

Rect getQuadExtent() {
	return { -100, -100, 100, 100 };
}

QuadTree makeQuadTree() {
	QuadTree quad{ getQuadExtent() };
	quad.insert({ 20, -30, 30, -20 }, 1);
	quad.insert({ -90, -90, -80, -80 }, 2);
	quad.insert({ -40, 15, -25, 30 }, 3);
	quad.insert({ -50, 60, -30, 80 }, 4);
	quad.insert({ -100, 5, -95, 10 }, 5);
	quad.insert({ -50, 40, -40, 50 }, 6);
	quad.insert({ -70, 30, -60, 40 }, 7);

	return quad;
}

using Clock = std::chrono::high_resolution_clock;
using Duration = std::chrono::duration<f32>;
using Timepoint = Clock::time_point;

int main(int argc, char** argv) {
	Window window{};
	World world{};
	world.createAgents();

	Timepoint prev = Clock::now();
	while (!window.isClosed()) {
		const Timepoint current = Clock::now();
		const Duration deltaTime = current - prev;
		prev = current;
		
		world.update(deltaTime.count());
		window.render(world.getQuadTree());
	}
	return 0;
}