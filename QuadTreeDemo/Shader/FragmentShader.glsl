#version 330 core


in vec2 TexCoord;
out vec4 FragColor;

uniform sampler2D textureSampler;
uniform vec3 minFilter;
uniform vec3 maxFilter;
uniform vec3 clearColor;

vec3 rgb2hsv(vec3 c) {
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

void main() {
    vec4 TexColor = texture(textureSampler, TexCoord);
    vec3 HSV = rgb2hsv(TexColor.xyz);
    vec3 minHSV = rgb2hsv(minFilter);
    vec3 maxHSV = rgb2hsv(maxFilter);

    if (minHSV.x <= HSV.x && minHSV.y <= HSV.y && minHSV.z <= HSV.z && 
        HSV.x <= maxHSV.x && HSV.y <= maxHSV.y && HSV.z <= maxHSV.z) {
        FragColor = TexColor;
    }
    else {
        FragColor = vec4(clearColor, 1.0);
    }
}