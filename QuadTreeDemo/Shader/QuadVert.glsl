#version 330 core

layout (location = 0) in vec2 vertPos;
layout (location = 1) in vec2 instanceTopLeft;
layout (location = 2) in vec2 instanceBottomRight;
layout (location = 3) in vec4 instanceColor;

uniform mat4 proj;

out vec4 fragColor;

void main()
{
	fragColor = instanceColor;
	vec2 instanceSize = instanceBottomRight - instanceTopLeft;
	vec2 scaledPos = vertPos * instanceSize;
	vec2 translatedPos = scaledPos + instanceTopLeft + 0.5 * instanceSize;
//	vec2 translatedPos = scaledPos + instancePos;
	gl_Position = proj * vec4(translatedPos, 0.0, 1.0);
}