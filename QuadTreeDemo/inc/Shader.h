#pragma once

#include "Common.h"
#include "FileSystem.h"

#include <string>
#include <vector>
#include <fstream>

#include <glm/glm.hpp>
#include <glm/vec4.hpp>

namespace Rw
{


	enum class ShaderType : u32 {
		Undefined = 0,
		Vertex,
		Fragment,
		Geometry
	};

	constexpr std::size_t NumShaderTypes = 3;

	class Shader {

	private:

		std::string source;
		std::string sourceFile;
		std::ostream* logStream;
		ShaderType type;
		u32 id;

	public:

		Shader() = default;
		Shader(std::string_view sourceFile, std::ostream& logStream, ShaderType type);

		Shader(const Shader&) = default;
		Shader& operator=(const Shader&) = default;

		Shader(Shader&&) noexcept;
		Shader& operator=(Shader&&) noexcept;

		~Shader();

		Err compile();

		inline u32 getId() const noexcept {
			return id;
		}

		inline bool isInitialized() const noexcept {
			return !sourceFile.empty();
		}

	};

	class ShaderProgram {

	private:

		std::vector<Shader> shaders;
		std::ofstream logStream;
		u32 id;

	public:

		ShaderProgram();
		~ShaderProgram();

		ShaderProgram& addShader(ShaderType type, Fs::Path sourceFile);
		Err build();
		Err validate();

		void use() const noexcept;

		ShaderProgram& setSampler(std::string_view name, int value) noexcept;
		ShaderProgram& setFloat(std::string_view name, f32 f) noexcept;
		ShaderProgram& setVec4(std::string_view name, f32 x, f32 y, f32 z, f32 w) noexcept;
		ShaderProgram& setVec3(std::string_view name, f32 x, f32 y, f32 z) noexcept;
		ShaderProgram& setVec2(std::string_view name, f32 x, f32 y) noexcept;

		ShaderProgram& setMat4(std::string_view name, const glm::mat4& mat) noexcept;

		inline ShaderProgram& setVec4(std::string_view name, const glm::vec4& vec) noexcept {
			return setVec4(name, vec.x, vec.y, vec.z, vec.w);
		}

		inline ShaderProgram& setVec3(std::string_view name, const glm::vec3& vec) noexcept {
			return setVec3(name, vec.x, vec.y, vec.z);
		}

		inline ShaderProgram& setVec2(std::string_view name, const glm::vec2& vec) noexcept {
			return setVec2(name, vec.x, vec.y);
		}

		inline ShaderProgram& setColor(std::string_view name, const glm::vec3& color) noexcept {
			return setVec3(name, color.r, color.g, color.b);
		}

		inline ShaderProgram& setColor(std::string_view name, f32 r, f32 g, f32 b) noexcept {
			return setVec3(name, r, g, b);
		}
	};

}