#pragma once

#include "Common.h"

#include <vector>
#include <variant>
#include <concepts>
#include <iterator>

template <typename T>
concept Trivial = std::is_trivial_v<T>;

namespace Rw
{
	template <typename T>
		requires Trivial<T>
	class FreeList {

	public:

		[[ nodiscard ]] i32 size() const noexcept { return static_cast<i32>(elements.size()); }
		[[ nodiscard ]] bool isEmpty() const noexcept { return elements.empty(); }

		void clear() noexcept;

		[[ nodiscard ]] T& operator[](i32 index) { return std::get<T>(elements[index]); }
		[[ nodiscard ]] const T& operator[](i32 index) const { return std::get<T>(elements[index]); }

		i32 insert(const T& element);
		void erase(i32 index) noexcept;

		bool checkFreeSlot(i32 index);

	private:

		using Element = std::variant<T, i32>;
		std::vector<Element> elements;
		i32 firstFree = NullIndex;

	public:

		class Iterator {

		public:

			using iterator_category = std::bidirectional_iterator_tag;
			using value_type = T;
			using difference_type = std::ptrdiff_t;
			using pointer = T*;
			using reference = T&;
			using const_reference = const T&;
			using container = FreeList<T>;

			Iterator(const Element* ptr, const container* freeList) noexcept
				: current{ ptr }, freeList{ freeList }
			{
				const Element* end = freeList->elements.data() + freeList->elements.size();
				while(current && current != end && !std::holds_alternative<T>(*current)) {
					++current;
				} 
			}

			Iterator& operator++() noexcept
			{
				const Element* end = freeList->elements.data() + freeList->elements.size();
				do {
					++current;
				} while (current != end && !std::holds_alternative<T>(*current));
				return *this;
			}

			Iterator operator++(int) noexcept
			{
				Iterator tmp = *this;
				++(*this);
				return tmp;
			}

			Iterator& operator--() noexcept
			{
				Iterator begin = freeList->begin();
				do {
					--current;
				} while (current != begin.current && !std::holds_alternative<T>(*current));
				return *this;
			}

			Iterator operator--(int) noexcept
			{
				Iterator tmp = *this;
				--(*this);
				return tmp;
			}

			const_reference operator*() const noexcept
			{
				return std::get<T>(*current);
			}

			const pointer operator->() const noexcept
			{
				return &std::get<T>(*current);
			}

			bool operator==(const Iterator& other) const noexcept
			{
				return current == other.current;
			}

			bool operator!=(const Iterator& other) const noexcept
			{
				return !(*this == other);
			}


		private:

			const Element* current;
			const container* freeList;

		};

		using value_type = T;
		using pointer = T*;
		using reference = T&;
		using const_reference = const T&;
		using difference_type = std::ptrdiff_t;
		using size_type = std::size_t;

		using iterator = Iterator;
		using const_iterator = Iterator;
		using reverse_iterator = std::reverse_iterator<iterator>;
		using const_reverse_iterator = std::reverse_iterator<const_iterator>;


		iterator begin() const noexcept {
			return iterator { elements.data(), this };
		}

		iterator end() const noexcept {
			const Element* ptr = elements.data() + elements.size();
			return iterator { ptr, this };
		}

	};


	template <typename T>
		requires Trivial<T>
	bool FreeList<T>::checkFreeSlot(i32 index) {
		return std::holds_alternative<i32>(elements[index]);
	}

	template <typename T>
		requires Trivial<T>
	void FreeList<T>::clear() noexcept {
		elements.clear();
		firstFree = NullIndex;
	}

	template <typename T>
		requires Trivial<T>
	i32 FreeList<T>::insert(const T& element) {
		i32 index = firstFree;
		if (index != NullIndex) {
			const i32 nextFree = std::get<i32>(elements[index]);
			firstFree = nextFree;
			elements[index] = element;
		}
		else {
			elements.push_back(element);
			index = size() - 1;
		}

		return index;
	}

	template <typename T>
		requires Trivial<T>
	void FreeList<T>::erase(i32 index) noexcept {
		elements[index] = firstFree;
		firstFree = index;
	}
}