#pragma once

#include "Common.h"
#include <glm/vec4.hpp>

class GLFWwindow;

namespace Rw
{
	class QuadTree;

	class Window {
	public:

		[[ nodiscard ]] Window() noexcept;
		~Window();

		void onResize(GLFWwindow* window, i32 width, i32 height) noexcept;
		void render(const QuadTree& tree) noexcept;
		
		[[ nodiscard ]] bool isClosed() const noexcept;

	private:

		GLFWwindow* wnd;
		i32 width, height;
		glm::vec4 backgroundColor;

		void clear() noexcept;

	};

}