#pragma once

#include <Common.h>
#include <concepts>

namespace Rw
{

namespace Math
{

	[[ nodiscard ]] constexpr auto powerOfTwo(std::integral auto exp) noexcept {
		return 1 << exp;
	}

}

}