#pragma once 

#include <cstdint>
#include <concepts>
#include <bit>

namespace Rw
{
	using u8 = std::uint8_t;
	using u16 = std::uint16_t;
	using u32 = std::uint32_t;
	using u64 = std::uint64_t;

	using i8 = std::int8_t;
	using i16 = std::int16_t;
	using i32 = std::int32_t;
	using i64 = std::int64_t;

	using f32 = float;
	using f64 = double;

	enum class Err : u8 {
		OK = 0,
		Warn,
		Error,
		Failure,
	};

	struct Rect {
		i32 left;
		i32 top; 
		i32 right;
		i32 bottom;
	};

	[[ nodiscard ]] constexpr bool isIntersecting(const Rect& r1, const Rect& r2) noexcept {
		return r1.left < r2.right && r1.right > r2.left &&
			r1.top < r2.bottom && r1.bottom > r2.top;
	}

	[[ nodiscard ]] constexpr bool operator==(const Rect& lhs, const Rect& rhs) noexcept {
		return lhs.left == rhs.left && lhs.top == rhs.top &&
			lhs.right == rhs.right && lhs.bottom == rhs.bottom;
	}

	constexpr i32 NullIndex = -1;
}