#pragma once

#include "Common.h"
#include "FreeList.h"
#include "SmallVector.h"
#include <concepts>
#include <vector>
#include <memory>
#include <functional>

namespace Rw
{
	using CollisionHandler = std::function<void(i32, i32)>;

	class QuadTree {
	public:

		QuadTree(const Rect& extent);
		QuadTree(const Rect& extent, i32 elementLimit);
		
		QuadTree(const QuadTree& other);
		QuadTree& operator=(const QuadTree& other);

		QuadTree(QuadTree&& other) noexcept;
		QuadTree& operator=(QuadTree&& other) noexcept;

		~QuadTree();

#ifndef RW_TEST_EXE
		void render(f32 width, f32 height) const;
#endif

		void insert(const Rect& rect, i32 id = -1);
		bool erase(const Rect& rect, i32 id);
		void cleanupEmptyLeaves();

		[[ nodiscard ]] bool testIntersection(const Rect& rect) const;
		[[ nodiscard ]] SmallVector<i32> queryIntersection(const Rect& rect, i32 id = NullIndex) const;

		void onCollision(const CollisionHandler& colHandler);
		void checkCollisions() const;

	private:

		static constexpr i32 MaxDepth = 8;
		static constexpr i32 BranchNode = -1;
		static constexpr i32 FreeNode	= -2;

		struct Node {
			// first element for leaves, first child for branches, next free for freed nodes
			i32 firstChild = NullIndex; 
			// number of elements for leaves, -1 otherwise
			i32 count = 0; 
		};

		struct NodeData {
			Rect rect;
			i32 index;
			i32 depth;
		};

		struct Element {
			Rect rect;
			i32 id;
		};

		struct ElementNode {
			// next element in leaf
			i32 next; 
			// element index
			i32 element;
		};

#ifndef RW_TEST_EXE
		class QuadTreeRenderer;
		using RendererPtr = std::unique_ptr<QuadTreeRenderer>;
#endif

		std::vector<Node> nodes;
		FreeList<Element> elements;
		FreeList<ElementNode> elementNodes;
		CollisionHandler handler;
		Rect rootExtent;
#ifndef RW_TEST_EXE
		RendererPtr renderer;
#endif
		i32 firstFree;
		i32 elementLimit;

		[[ nodiscard ]] i32 createOrRecycleNodes();
		void splitLeaf(NodeData& leafData);
		
		[[ nodiscard ]] SmallVector<i32> gatherLeaves() const;
		void addCollisions(i32 elementId, SmallVector<i32>& intersections, std::vector<SmallVector<i32>>& collisionList) const;
	};

}

