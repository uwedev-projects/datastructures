#pragma once

#include "Common.h"
#include "QuadTree.h"

#include <vector>
#include <glm/vec2.hpp>

namespace Rw
{
	struct Agent {
		glm::vec2 topLeft;
		glm::vec2 botRight;
		glm::vec2 velocity;
		//f32 mass;
	};

	class World {

	public:

		World();
		
		void createAgents();
		void update(f32 dt);
		void moveAgent(Agent& agent, f32 dt);

		[[ nodiscard ]] inline const QuadTree& getQuadTree() const noexcept {
			return tree;
		}


	private:

		QuadTree tree;
		std::vector<Agent> agents;
	};

}