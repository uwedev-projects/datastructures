#pragma once 

#include "Common.h"
#include <algorithm>
#include <concepts>
#include <optional>
#include <ranges>
#include <variant>
#include <vector>
#include <array>

#include <cassert>

namespace Rw
{
	template <typename T>
	concept Trivial = std::is_trivial_v<T>;

	template <typename T>
		requires Trivial<T>
	class SmallVector {
	public:

		static constexpr const i32 SmallCapacity = 128;

		void push(const T& element);
		[[ nodiscard ]] const T& top() const noexcept;
		T pop();

		[[ nodiscard ]] bool isSmall() const noexcept { return smallSize >= 0; }
		[[ nodiscard ]] bool isEmpty() const noexcept { return isSmall() ? smallSize == 0 : std::get<BigStorage>(storage).empty(); }
		[[ nodiscard ]] std::size_t size() const noexcept { return isSmall() ? smallSize : std::get<BigStorage>(storage).size(); }

		[[ nodiscard ]] T& operator[](std::size_t index) noexcept;
		[[ nodiscard ]] const T& operator[](std::size_t index) const noexcept {
			return operator[](index);
		}

		[[ nodiscard ]] bool contains(const T& value) const noexcept;

		SmallVector() noexcept = default;
		SmallVector(const T& initVal, std::size_t count);

	private:

		using SmallStorage = std::array<T, SmallCapacity>;
		using BigStorage = std::vector<T>;
		using Storage = std::variant<SmallStorage, BigStorage>;

		Storage storage{ SmallStorage{} };
		i32 smallSize{ 0 };

	};

	template <typename T> requires Trivial<T>
	SmallVector<T>::SmallVector(const T& initVal, std::size_t count) :
		storage{ count < SmallCapacity ? SmallStorage{} : BigStorage{} },
		smallSize{ count < SmallCapacity ? static_cast<i32>(count) : -1}
	{
		if (isSmall()) {
			SmallStorage& small = std::get<SmallStorage>(storage);
			std::fill_n(small.begin(), count, initVal);
		}
		else {
			BigStorage& big = std::get<BigStorage>(storage);
			big.reserve(count);
			std::fill_n(big.begin(), count, initVal);
		}
		
	}

	template <typename T> requires Trivial<T>
	void SmallVector<T>::push(const T& element) {
		if (isSmall() && smallSize < SmallCapacity) [[ likely ]] {
			SmallStorage& small = std::get<SmallStorage>(storage);
			small[smallSize] = element;
			smallSize = std::min(smallSize + 1, SmallCapacity);
			assert(smallSize <= SmallCapacity);
		}
		else if (smallSize == SmallCapacity) [[ unlikely ]] {
			smallSize = -1;
			const SmallStorage small{ std::move(std::get<SmallStorage>(storage)) };
			storage = BigStorage{};
			BigStorage& big = std::get<BigStorage>(storage);
			big.reserve(SmallCapacity + 1);
			std::ranges::copy(small.begin(), small.end(), std::back_inserter(big));
			big.push_back(element);
		}
		else {
			BigStorage& big = std::get<BigStorage>(storage);
			big.push_back(element);
		}
	}

	template <typename T> requires Trivial<T>
	const T& SmallVector<T>::top() const noexcept {
		const T* result = nullptr;
		if (isSmall()) [[ likely ]] {
			const SmallStorage& small = std::get<SmallStorage>(storage);
			result = &small[smallSize - 1];
		}
		else [[ unlikely ]] {
			const BigStorage& big = std::get<BigStorage>(storage);
			result = &big.back();
		}

		return *result;
	}

	template <typename T> requires Trivial<T>
	T SmallVector<T>::pop() {
		std::optional<T> result;
		if (isSmall()) [[ likely ]] {
			SmallStorage& small = std::get<SmallStorage>(storage);
			assert(smallSize <= SmallCapacity);
			result = small[--smallSize];
		}
		else [[ unlikely ]] {
			BigStorage& big = std::get<BigStorage>(storage);
			result = big.back();
			big.pop_back();
		}
		return result.value();
	}

	template <typename T> requires Trivial<T>
	T& SmallVector<T>::operator[](std::size_t index) noexcept {
		T* result = nullptr;

		if (isSmall()) [[ likely ]] {
			SmallStorage& small = std::get<SmallStorage>(storage);
			result = &small[index];
		} 
		else [[ unlikely ]] {
			BigStorage& big = std::get<BigStorage>(storage);
			result = &big[index];
		}

		return *result;
	}

	template <typename T> requires Trivial<T>
	bool SmallVector<T>::contains(const T& value) const noexcept {
		bool found = false; 

		if (isSmall()) [[ likely ]] {
			const SmallStorage& small = std::get<SmallStorage>(storage);
			found = std::ranges::contains(small, value);
		}
		else [[ unlikely ]] {
			const BigStorage& big = std::get<BigStorage>(storage);
			found = std::ranges::contains(big, value);
		}

		return found;
	}
}