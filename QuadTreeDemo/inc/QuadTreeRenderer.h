#pragma once

#include "QuadTree.h"
#include "Shader.h"
#include "RwMath.h"

#include <vector>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>

namespace Rw
{

	struct RenderRect {
		std::vector<glm::vec2> vertices;
		std::vector<u32> indices;
		u32 vertexBufferId;
		u32 vertexArrayId;
		u32 indexBufferId;
	};

	struct QuadInstance {
		glm::vec2 topLeft;
		glm::vec2 bottomRight;
		glm::vec4 color;
	};

	class QuadTree::QuadTreeRenderer {
	public:

		QuadTreeRenderer(const QuadTree* tree);
		~QuadTreeRenderer();

		void render(f32 width, f32 height);

	private:

		static constexpr i32 MaxQuadInstances = Math::powerOfTwo(QuadTree::MaxDepth) * Math::powerOfTwo(QuadTree::MaxDepth);

		glm::mat4 projection;
		ShaderProgram shader;
		RenderRect quadModel;
		RenderRect elementModel;
		std::vector<QuadInstance> quadInstances;
		std::vector<QuadInstance> elementInstances;
		const QuadTree* tree;
		u32 quadInstanceVBO;
		u32 elementInstanceVBO;

		void prepareQuadInstances();
		void prepareElementInstances();
		void updateQuadData();
		void updateElementData();

	};

}