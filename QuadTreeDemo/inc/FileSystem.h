#pragma once

#include "Common.h"

#include <vector>
#include <string>
#include <string_view>
#include <filesystem>

namespace Rw
{
	namespace Fs
	{
		using Path = std::filesystem::path;

		class FileHandle {
			
		private:

			std::FILE* file;
			std::string path;

		public:

			FileHandle(std::string_view filePath, std::string_view mode = "r+");
			FileHandle(Path filePath, std::string_view mode = "r+");

			template <typename T>
			std::size_t write(const T* data, std::size_t count = 1) {
				return this->write(std::bit_cast<const u8*, const T*>(data), sizeof(T), count);
			}

			std::size_t write(const u8* data, std::size_t size, std::size_t count);
			std::size_t write(std::string_view str);

			template <typename T>
			void read(T* data, std::size_t count = 1) {
				this->read(std::bit_cast<u8*, T*>(data), sizeof(T), count);
			}

			void read(u8* data, std::size_t size, std::size_t count);

			[[ nodiscard ]]
			std::vector<char> read();

			void flush() const noexcept;

			i32 getSize() const noexcept;
			i32 getCursorPos() const noexcept;

			~FileHandle();

		};
	}

}