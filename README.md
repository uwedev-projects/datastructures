# DataStructures - Practise Project #

I created this project as a space where can i learn and test some new concepts 
with regards to algorithms and data structures. I will probably come back and
explore different topics here from time to time. 

My first topic of interest were trees and their different applications.

Heres some demo footage of a quadtree collision detection which I built 
based on this stackoverflow post:

https://stackoverflow.com/questions/41946007/efficient-and-well-explained-implementation-of-a-quadtree-for-2d-collision-det

## Without quadtree grid: ##
![Alt text](/Gifs/quad_1000_agents.gif?raw=true "1000 agents without grid")

## With quadtree grid: ##
![Alt text](/Gifs/quad_1000_agents_with_sectors.gif?raw=true "1000 agents with grid")