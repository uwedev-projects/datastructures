#include <gtest/gtest.h>
#include "RedBlackTree.h"

#include <vector>
#include <ranges>
#include <utility>
#include <algorithm>

namespace Rw
{

	TEST(RedBlackTreeTest, InsertNewElements) {
		RedBlackTree<char, int> map{};

		std::vector<std::pair<char, int>> inputData{
			{'M', 1},
			{'J', 2},
			{'R', 3},
			{'E', 4},
			{'L', 5},
			{'P', 6},
			{'X', 7}
		};

		for (const std::pair<char, int>& keyValue : inputData) {
			const auto& [key, value] = keyValue;
			EXPECT_TRUE(map.insert(key, value));
		}

		std::ranges::sort(inputData, [](const std::pair<char, int>& first, const std::pair<char, int>& second) noexcept {
			return first.first < second.first;
		});

		for (auto itMap = map.begin(); const auto [expectedKey, expectedValue] : inputData) {
			const auto [key, value] = *itMap;
			EXPECT_EQ(key, expectedKey);
			EXPECT_EQ(value, expectedValue);
			++itMap;
		}

	}

	TEST(RedBlackTreeTest, FindElement) {
		RedBlackTree<char, int> map{};
		map.insert('E', 1);
		map.insert('A', 2);
		map.insert('B', 3);
		map.insert('D', 4);
		map.insert('K', 5);
		map.insert('U', 6);

		auto itSearch = map.find('D');
		EXPECT_NE(itSearch, map.end());
		EXPECT_EQ(itSearch->first, 'D');
		EXPECT_EQ(itSearch->second, 4);

		itSearch = map.find('U');
		EXPECT_NE(itSearch, map.end());
		EXPECT_EQ(itSearch->first, 'U');
		EXPECT_EQ(itSearch->second, 6);
	}

	TEST(RedBlackTreeTest, DeleteLeafNode) {
		RedBlackTree<char, int> map{};
		map.insert('R', 1);
		map.insert('E', 2);
		map.insert('T', 3);
		map.insert('C', 4);
		map.insert('M', 5);
		map.insert('H', 6);
		map.insert('A', 7);
		map.insert('D', 8);
		map.insert('S', 9);
		EXPECT_TRUE(map.erase('H'));
		EXPECT_EQ(map.find('H'), map.end());
		EXPECT_TRUE(map.isValidRedBlackTree());
	}

	TEST(RedBlackTreeTest, DeleteInternalNodeWithTwoChildren) {
		RedBlackTree<char, int> map{};
		map.insert('R', 1);
		map.insert('E', 2);
		map.insert('T', 3);
		map.insert('C', 4);
		map.insert('M', 5);
		map.insert('H', 6);
		map.insert('A', 7);
		map.insert('D', 8);
		map.insert('S', 9);
		EXPECT_TRUE(map.erase('R'));
		EXPECT_EQ(map.find('R'), map.end());
		EXPECT_TRUE(map.isValidRedBlackTree());
	}


	TEST(RedBlackTreeTest, DeleteInternalNodeWithOneChild) {
		RedBlackTree<char, int> map{};
		map.insert('R', 1);
		map.insert('E', 2);
		map.insert('T', 3);
		map.insert('C', 4);
		map.insert('M', 5);
		map.insert('H', 6);
		map.insert('A', 7);
		map.insert('D', 8);
		map.insert('S', 9);
		EXPECT_TRUE(map.erase('T'));
		EXPECT_EQ(map.find('T'), map.end());
		EXPECT_TRUE(map.isValidRedBlackTree());
	}

	TEST(RedBlackTreeTest, DeleteRootNode) {
		RedBlackTree<char, int> map{};
		map.insert('R', 1);
		map.insert('E', 2);
		map.insert('T', 3);
		map.insert('C', 4);
		map.insert('M', 5);
		map.insert('H', 6);
		map.insert('A', 7);
		map.insert('D', 8);
		map.insert('S', 9);
		EXPECT_TRUE(map.erase('E'));
		EXPECT_EQ(map.find('E'), map.end());
		EXPECT_TRUE(map.isValidRedBlackTree());
	}

}