#include <gtest/gtest.h>
#include "Heap.h"

#include <functional>

namespace Rw
{
	TEST(BinaryHeapTest, HeapConstructionFromPrimitiveInitializerList) {
		BinaryHeap<int> minHeap{ 5, 2, 8, -1, 10, 4 };
	}

	TEST(BinaryHeapTest, PopMinElement) {
		BinaryHeap<int> minHeap{ 6, -3, -2, 1, 14, -5, 2, 4 };
		EXPECT_EQ(minHeap.pop(), -5);
		EXPECT_EQ(minHeap.pop(), -3);
		EXPECT_EQ(minHeap.pop(), -2);
		EXPECT_EQ(minHeap.pop(), 1);
		EXPECT_EQ(minHeap.pop(), 2);
		EXPECT_EQ(minHeap.pop(), 4);
		EXPECT_EQ(minHeap.pop(), 6);
	}

	TEST(BinaryHeapTest, MinHeapPushNewElement) {
		BinaryHeap<int> minHeap{ -4, 2, 6 };

		minHeap.push(-5);
		EXPECT_EQ(minHeap.peek(), -5);

		minHeap.push(2);
		EXPECT_EQ(minHeap.peek(), -5);

		minHeap.push(-10);
		EXPECT_EQ(minHeap.peek(), -10);
	}

	TEST(BinaryHeapTest, EmptyMinHeap) {
		BinaryHeap<int> minHeap{};
		EXPECT_TRUE(minHeap.empty());

		minHeap.push(5);
		EXPECT_FALSE(minHeap.empty());
		
		minHeap.pop();
		EXPECT_TRUE(minHeap.empty());
	}

	TEST(BinaryHeapTest, HeapSize) {
		BinaryHeap<int> minHeap{};
		EXPECT_EQ(minHeap.size(), 0);

		minHeap.push(1);
		EXPECT_EQ(minHeap.size(), 1);

		minHeap.push(0);
		EXPECT_EQ(minHeap.size(), 2);

		minHeap.pop();
		EXPECT_EQ(minHeap.size(), 1);
	}

	using MaxHeap = BinaryHeap<int, std::greater<int>>;

	TEST(BinaryHeapTest, PopMaxElement) {
		MaxHeap heap{ 4, -2, 0, 1, 1, 5, 0, 5 };

		EXPECT_EQ(heap.pop(), 5);
		EXPECT_EQ(heap.pop(), 5);
		EXPECT_EQ(heap.pop(), 4);
		EXPECT_EQ(heap.pop(), 1);
		EXPECT_EQ(heap.pop(), 1);
		EXPECT_EQ(heap.pop(), 0);
		EXPECT_EQ(heap.pop(), 0);
		EXPECT_EQ(heap.pop(), -2);
	}

	TEST(BinaryHeapTest, MaxHeapPushNewElement) {
		MaxHeap heap{ 4 };
		
		heap.push(5);
		EXPECT_EQ(heap.peek(), 5);

		heap.push(2);
		EXPECT_EQ(heap.peek(), 5);

		heap.push(8);
		EXPECT_EQ(heap.peek(), 8);
	}
}