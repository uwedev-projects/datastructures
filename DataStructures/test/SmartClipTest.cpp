#include <gtest/gtest.h>
#include <iostream>
#include <utility>
#include <vector>
#include <map>

namespace Rw
{

	void printNumbers(const std::vector<int>& numbers) {
		for (const int num : numbers) {
			std::cout << num << ' ';
		}

		std::cout << '\n';
	}

	std::vector<int> getNumbers(int max) {
		std::vector<int> numbers{};
		numbers.reserve(max);
		for (int i = 1; i <= max; ++i) {
			if (i <= 20) {
				numbers.emplace_back(i * 2);
			}
			else if (i >= 40) {
				numbers.emplace_back(i % 2 == 0 ? i + 1 : i);
			}
			else {
				numbers.emplace_back(i);
			}
			
		}
		return numbers;
	}

	void eraseOddNumbers(std::vector<int>& numbers) {
		if (numbers.empty())
			return;

		bool done = false;
		std::size_t partitionPoint = numbers.size();
		for (std::size_t lo = 0, hi = numbers.size() - 1; !done;) {
			while (lo <= hi && numbers[lo] % 2 == 0) ++lo;
			while (hi > lo && numbers[hi] % 2 != 0) --hi;
			
			if (lo < numbers.size() && numbers[lo] % 2 != 0 && numbers[hi] % 2 == 0) {
				numbers[lo++] = std::move(numbers[hi--]);
			}

			if (hi <= lo) {
				partitionPoint = lo;
				done = true;
			}
		}

		if (partitionPoint < numbers.size()) {
			numbers.erase(numbers.begin() + partitionPoint, numbers.end());
		}
	}

	std::map<int, int> getEvenOccurences(const std::vector<int>& numbers) {
		std::map<int, int> occurences{};
		for (const int i : numbers) {
			if (i % 2 == 0) {
				occurences[i]++;
			}
		}

		return occurences;
	}

	TEST(SmartClipTest, BeginningEvenEndOdd) {
		std::vector<int> numbers{ 2, 4, 6, 7, 8, 9, 11, 13, 15 };

		const std::map<int, int> expectedEven{ getEvenOccurences(numbers) };
		eraseOddNumbers(numbers);
		const std::map<int, int> actualEven{ getEvenOccurences(numbers) };

		for (const int i : numbers) {
			EXPECT_TRUE(i % 2 == 0);
		}

		EXPECT_EQ(expectedEven.size(), actualEven.size());
		for (const auto [num, expectedCount] : expectedEven) {
			const int actualCount = actualEven.at(num);
			EXPECT_EQ(expectedCount, actualCount);
		}
	}


	TEST(SmartClipTest, BeginningEvenEndOdd2) {
		std::vector<int> numbers{ 2, 4, 6, 7, 8, 9, 11, 13, 15, 17 };

		const std::map<int, int> expectedEven{ getEvenOccurences(numbers) };
		eraseOddNumbers(numbers);
		const std::map<int, int> actualEven{ getEvenOccurences(numbers) };

		for (const int i : numbers) {
			EXPECT_TRUE(i % 2 == 0);
		}

		EXPECT_EQ(expectedEven.size(), actualEven.size());
		for (const auto [num, expectedCount] : expectedEven) {
			const int actualCount = actualEven.at(num);
			EXPECT_EQ(expectedCount, actualCount);
		}
	}

	TEST(SmartClipTest, BeginningOddEndEven) {
		std::vector<int> numbers{ 1, 3, 5, 7, 8, 10, 12, 14 };

		const std::map<int, int> expectedEven{ getEvenOccurences(numbers) };
		eraseOddNumbers(numbers);
		const std::map<int, int> actualEven{ getEvenOccurences(numbers) };

		for (const int i : numbers) {
			EXPECT_TRUE(i % 2 == 0);
		}

		EXPECT_EQ(expectedEven.size(), actualEven.size());
		for (const auto [num, expectedCount] : expectedEven) {
			const int actualCount = actualEven.at(num);
			EXPECT_EQ(expectedCount, actualCount);
		}
	}

	TEST(SmartClipTest, BeginningOddEndEven2) {
		std::vector<int> numbers{ 1, 3, 5, 7, 8, 10, 12 };

		const std::map<int, int> expectedEven{ getEvenOccurences(numbers) };
		eraseOddNumbers(numbers);
		const std::map<int, int> actualEven{ getEvenOccurences(numbers) };

		for (const int i : numbers) {
			EXPECT_TRUE(i % 2 == 0);
		}

		EXPECT_EQ(expectedEven.size(), actualEven.size());
		for (const auto [num, expectedCount] : expectedEven) {
			const int actualCount = actualEven.at(num);
			EXPECT_EQ(expectedCount, actualCount);
		}
	}

	TEST(SmartClipTest, MixedNumbers) {
		std::vector<int> numbers{ 1, 2, 2, 3, 5, 1, 4, 11, 4, 9, 20 };

		const std::map<int, int> expectedEven{ getEvenOccurences(numbers) };
		eraseOddNumbers(numbers);
		const std::map<int, int> actualEven{ getEvenOccurences(numbers) };

		for (const int i : numbers) {
			EXPECT_TRUE(i % 2 == 0);
		}

		EXPECT_EQ(expectedEven.size(), actualEven.size());
		for (const auto [num, expectedCount] : expectedEven) {
			const int actualCount = actualEven.at(num);
			EXPECT_EQ(expectedCount, actualCount);
		}
	}

	TEST(SmartClipTest, AscendingNumbers) {
		std::vector<int> numbers{ 1, 2, 3, 4, 5, 6, 7};
		const std::map<int, int> expectedEven{ getEvenOccurences(numbers) };
		eraseOddNumbers(numbers);
		const std::map<int, int> actualEven{ getEvenOccurences(numbers) };

		for (const int i : numbers) {
			EXPECT_TRUE(i % 2 == 0);
		}

		EXPECT_EQ(expectedEven.size(), actualEven.size());
		for (const auto [num, expectedCount] : expectedEven) {
			const int actualCount = actualEven.at(num);
			EXPECT_EQ(expectedCount, actualCount);
		}
	}

	TEST(SmartClipTest, AscendingNumbers2) {
		std::vector<int> numbers{ 1, 2, 3, 4, 5, 6 };
		const std::map<int, int> expectedEven{ getEvenOccurences(numbers) };
		eraseOddNumbers(numbers);
		const std::map<int, int> actualEven{ getEvenOccurences(numbers) };

		for (const int i : numbers) {
			EXPECT_TRUE(i % 2 == 0);
		}

		EXPECT_EQ(expectedEven.size(), actualEven.size());
		for (const auto [num, expectedCount] : expectedEven) {
			const int actualCount = actualEven.at(num);
			EXPECT_EQ(expectedCount, actualCount);
		}
	}

	TEST(SmartClipTest, AllNumbersOdd) {
		std::vector<int> numbers{ 1, 1, 3, 3, 5, 5 };
		eraseOddNumbers(numbers);
		EXPECT_EQ(numbers.size(), 0);

		numbers = { 1, 1, 3, 3, 5 };
		eraseOddNumbers(numbers);
		EXPECT_EQ(numbers.size(), 0);
	}

	TEST(SmartClipTest, AllNumbersEven) {
		std::vector<int> numbers{ 2, 4, 6, 8, 10, 12 };
		eraseOddNumbers(numbers);
		EXPECT_EQ(numbers.size(), 6);

		numbers = { 2, 4, 6, 8, 10 };
		eraseOddNumbers(numbers);
		EXPECT_EQ(numbers.size(), 5);
	}

	TEST(SmartClipTest, NoNumbers) {
		std::vector<int> numbers{};
		eraseOddNumbers(numbers);
		EXPECT_TRUE(numbers.empty());
	}

	TEST(SmartClipTest, OneOddNumber) {
		std::vector<int> numbers{ 3 };
		eraseOddNumbers(numbers);
		EXPECT_TRUE(numbers.empty());
	}

	TEST(SmartClipTest, OneEvenNumber) {
		std::vector<int> numbers{ 4 };
		eraseOddNumbers(numbers);
		EXPECT_EQ(numbers.size(), 1);
		EXPECT_EQ(numbers.front(), 4);
	}
}