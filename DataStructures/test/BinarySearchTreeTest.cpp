#include <gtest/gtest.h>
#include "BinarySearchTree.h"

#include <vector>
#include <ranges>
#include <utility>
#include <algorithm>

namespace Rw
{

	TEST(BinarySearchTreeTest, ForwardTraversal) {
		BinarySearchTree<char, int> map{};
		std::vector<std::pair<char, int>> inputData{
			{'G', 4},
			{'C', 10},
			{'J', 7},
			{'A', 3},
			{'K', 12},
			{'D', 20},
			{'I', 1}
		};

		for (const std::pair<char, int>& keyValue : inputData) {
			const auto& [key, value] = keyValue;
			map.insert(key, value);
		}

		std::ranges::sort(inputData, [](const std::pair<char, int>& first, const std::pair<char, int>& second) noexcept {
			return first.first < second.first;
		});

		auto itForward = map.begin();
		for (std::size_t i = 0; i < inputData.size(); ++i, ++itForward) {
			const auto& [key, value] = *itForward;
			EXPECT_EQ(key, inputData[i].first);
			EXPECT_EQ(value, inputData[i].second);
		}

		EXPECT_EQ(itForward, map.end());
	}

	TEST(BinarySearchTreeTest, ReverseTraversal) {
		BinarySearchTree<char, int> map{};
		std::vector<std::pair<char, int>> inputData{
			{'H', 4},
			{'D', 10},
			{'C', 7},
			{'Z', 17},
			{'K', 5},
			{'J', 15}
		};

		for (const std::pair<char, int>& keyValue : inputData) {
			const auto& [key, value] = keyValue;
			map.insert(key, value);
		}

		std::ranges::sort(inputData, [](const std::pair<char, int>& first, const std::pair<char, int>& second) noexcept {
			return first.first > second.first;
		});

		auto itReverse = map.rbegin();
		for (std::size_t i = 0; i < inputData.size(); ++i, ++itReverse) {
			const auto& [key, value] = *itReverse;
			EXPECT_EQ(key,   inputData[i].first);
			EXPECT_EQ(value, inputData[i].second);
		}

		EXPECT_EQ(itReverse, map.rend());
	}

	TEST(BinarySearchTreeTest, FindFirstElementByKey) {
		BinarySearchTree<char, int> map{};
		map.insert('T', 4);
		map.insert('C', 2);
		map.insert('Y', 5);
		map.insert('U', 1);
		map.insert('L', 8);

		const auto itResult = map.find('C');
		const auto& [key, value] = *itResult;
		EXPECT_EQ(key, 'C');
		EXPECT_EQ(value, 2);
		EXPECT_EQ(itResult, map.begin());
	}

	TEST(BinarySearchTreeTest, FindLastElementByKey) {
		BinarySearchTree<char, int> map{};
		map.insert('T', 4);
		map.insert('C', 2);
		map.insert('Y', 5);
		map.insert('U', 1);
		map.insert('L', 8);

		auto itResult = map.find('Y');
		const auto& [key, value] = *itResult;
		EXPECT_EQ(key, 'Y');
		EXPECT_EQ(value, 5);
		EXPECT_EQ(++itResult, map.end());
	}

	TEST(BinarySearchTreeTest, EraseLeafNode) {
		BinarySearchTree<char, int> map{};
		map.insert('S', 1);
		map.insert('E', 2);
		map.insert('X', 3);
		map.insert('A', 4);
		map.insert('R', 5);
		map.insert('C', 6);
		map.insert('H', 7);
		map.insert('M', 8);

		EXPECT_TRUE(map.erase('C'));
		EXPECT_EQ(map.find('C'), map.end());

		EXPECT_TRUE(map.erase('X'));
		EXPECT_EQ(map.find('X'), map.end());

		EXPECT_TRUE(map.erase('A'));
		EXPECT_EQ(map.find('A'), map.end());
	}

	TEST(BinarySearchTreeTest, EraseNodeWithSingleChild) {
		BinarySearchTree<char, int> map{};
		map.insert('S', 1);
		map.insert('E', 2);
		map.insert('X', 3);
		map.insert('A', 4);
		map.insert('R', 5);
		map.insert('C', 6);
		map.insert('H', 7);
		map.insert('M', 8);

		EXPECT_TRUE(map.erase('A'));
		EXPECT_EQ(map.find('A'), map.end());
		EXPECT_NE(map.find('C'), map.end());

		EXPECT_TRUE(map.erase('R'));
		EXPECT_EQ(map.find('R'), map.end());
		EXPECT_NE(map.find('H'), map.end());
		EXPECT_NE(map.find('M'), map.end());
	}

	TEST(BinarySearchTreeTest, EraseNodeWithTwoChildren) {
		BinarySearchTree<char, int> map{};
		map.insert('S', 1);
		map.insert('E', 2);
		map.insert('V', 3);
		map.insert('A', 4);
		map.insert('R', 5);
		map.insert('C', 6);
		map.insert('H', 7);
		map.insert('M', 8);
		map.insert('T', 9);
		map.insert('X', 10);
		map.insert('W', 11);

		EXPECT_TRUE(map.erase('E'));
		EXPECT_EQ(map.find('E'), map.end());
		EXPECT_NE(map.find('H'), map.end());
		EXPECT_NE(map.find('R'), map.end());
		EXPECT_NE(map.find('M'), map.end());

		EXPECT_TRUE(map.erase('V'));
		EXPECT_EQ(map.find('V'), map.end());
		EXPECT_NE(map.find('W'), map.end());
		EXPECT_NE(map.find('T'), map.end());
		EXPECT_NE(map.find('X'), map.end());
	}

	TEST(BinarySearchTreeTest, EraseRootNode) {
		BinarySearchTree<char, int> map{};
		map.insert('S', 1);
		map.insert('E', 2);
		map.insert('X', 3);
		map.insert('A', 4);
		map.insert('R', 5);
		map.insert('C', 6);
		map.insert('H', 7);
		map.insert('M', 8);

		EXPECT_TRUE(map.erase('S'));
		EXPECT_EQ(map.find('S'), map.end());
		EXPECT_NE(map.find('X'), map.end());
		EXPECT_NE(map.find('E'), map.end());
	}

	//TEST(BinarySearchTreeTest, ReverseIterationAfterErasingMaxNode) {

	//}

}