#pragma once

#include <stack>
#include <vector>
#include <memory>
#include <cassert>
#include <utility>
#include <concepts>
#include <functional>

namespace Rw
{
	template <typename CompareFn, typename T>
	concept NoThrowComparable = requires (CompareFn compare, const T & arg1, const T & arg2) {
		{ compare(arg1, arg2) } noexcept;
	};

	template <typename T>
	concept NoThrowDefaultConstructible = requires {
		{ T{} } noexcept;
	};

	template <typename Key, typename Val, typename CompareFn = std::less<Key>>
		requires std::default_initializable<Key>&& std::default_initializable<Val>
	&& std::strict_weak_order<CompareFn, const Key&, const Key&>
	class RedBlackTree {
	private:

		struct Node;
		using NodePtr = std::unique_ptr<Node>;

		enum class Color : unsigned char { Red, Black };

		struct Node {
			std::pair<Key, Val> keyValue;
			NodePtr left;
			NodePtr right;
			Node* parent;
			Color color;
		};

		NodePtr root;
		CompareFn compare;

		using Stack = std::stack<Node*, std::vector<Node*>>;

		[[ nodiscard ]]
		Node* search(const Key& key) const noexcept (NoThrowComparable<CompareFn, Key>) {
			Node* result{};
			if (root == nullptr)
				return result;

			Node* current{ root.get() };
			while (current != nullptr) {
				const bool isBefore = compare(key, current->keyValue.first);

				if (isBefore) {
					current = current->left ? current->left.get() : nullptr;
					continue;
				}

				const bool isAfter = compare(current->keyValue.first, key);

				if (isAfter) {
					current = current->right ? current->right.get() : nullptr;
				}
				else {
					result = current;
					current = nullptr;
				}
			}

			return result;
		}

		[[ nodiscard ]]
		Node* getMinNode(Node* subtree) const noexcept {
			Node* minNode = subtree;
			while (minNode->left != nullptr) {
				minNode = minNode->left.get();
			}
			return minNode;
		}

		[[ nodiscard ]]
		Node* getMaxNode(Node* subtree) const noexcept {
			Node* maxNode = subtree;
			while (maxNode->right != nullptr) {
				maxNode = maxNode->right.get();
			}
			return maxNode;
		}

		[[ nodiscard ]]
		int getBlackHeight(Node* subtree) const noexcept {
			Node* current = subtree;
			int blackHeight = current->color == Color::Black ? 1 : 0;
			while (current->left != nullptr) {
				current = current->left.get();
				if (current == nullptr || current->color == Color::Black)
					++blackHeight;
			}
			return blackHeight;
		}
	
		Node* rotateLeft(Node* target) noexcept {
			assert(target->right && target->right->color == Color::Red);
			
			Node* newRoot = target->right.release();
			if (newRoot->left != nullptr)
				target->right = std::move(newRoot->left);

			Node* parent = target->parent;
			if (parent) {
				NodePtr& child = parent->left != nullptr && parent->left.get() == target ? parent->left : parent->right;
				newRoot->left = std::move(child);
				newRoot->parent = parent;
				child = NodePtr{ newRoot };
			}
			else {
				newRoot->left = std::move(root);
				newRoot->parent = nullptr;
				root = NodePtr{ newRoot };
			}

			newRoot->color = target->color;
			target->color = Color::Red;
			target->parent = newRoot;
			return newRoot;
		}

		Node* rotateRight(Node* target) noexcept {
			assert(target->left && target->left->color == Color::Red);

			Node* newRoot = target->left.release();
			if (newRoot->right != nullptr)
				target->left = std::move(newRoot->right);

			Node* parent = target->parent;
			if (parent) {
				NodePtr& child = parent->left != nullptr && parent->left.get() == target ? parent->left : parent->right;
				newRoot->right = std::move(child);
				newRoot->parent = parent;
				child = NodePtr{ newRoot };
			}
			else {
				newRoot->right = std::move(root);
				newRoot->parent = nullptr;
				root = NodePtr{ newRoot };
			}

			newRoot->color = target->color;
			target->color = Color::Red;
			target->parent = newRoot;
			return newRoot;
		}

		bool isRed(const NodePtr& node) const noexcept {
			return node != nullptr && node->color == Color::Red;
		}

		void flipColors(Node* target) noexcept {
			target->color = target->color == Color::Black && target != root.get() ? Color::Red : Color::Black;

			if (target->left != nullptr)
				target->left->color  = target->left->color == Color::Black ? Color::Red : Color::Black;

			if (target->right != nullptr)
				target->right->color = target->right->color == Color::Black ? Color::Red : Color::Black;
		}

		Node* moveRedLeft(Node* target) noexcept {
			flipColors(target);
			if (target->right && isRed(target->right->left)) {
				rotateRight(target->right.get());
				target = rotateLeft(target);
				flipColors(target);
			}

			return target;
		}

		Node* moveRedRight(Node* target) noexcept {
			flipColors(target);
			if (target->left && isRed(target->left->left)) {
				target = rotateRight(target);
				flipColors(target);
			}

			return target;
		}

		void rebalance(Stack& visited) {
			while (!visited.empty()) {
				Node* current = visited.top();

				if (!isRed(current->left) && isRed(current->right)) {
					current = rotateLeft(current);
				}

				if (isRed(current->left) && current->left->left != nullptr && isRed(current->left->left)) {
					current = rotateRight(current);
				}

				if (isRed(current->left) && isRed(current->right)) {
					flipColors(current);
				}

				visited.pop();
			}
		}

		void deleteMinNode(Node* subtree) {
			Stack visited{};
			Node* current = subtree;
			while (current->left != nullptr) {
				if (!isRed(current->left) && !isRed(current->left->left))
					current = moveRedLeft(current);

				visited.push(current);
				current = current->left ? current->left.get() : nullptr;
			}

			Node* parent = current->parent;
			NodePtr& child = parent->left && parent->left.get() == current ? parent->left : parent->right;
			current = nullptr;
			child.reset();

			rebalance(visited);
		}

	public:

		template <bool Const = false>
		class Iterator;

		using value_type = std::pair<Key, Val>;
		using pointer = value_type*;
		using reference = value_type&;
		using const_reference = const value_type&;
		using iterator = Iterator<>;
		using const_iterator = Iterator<true>;
		using difference_type = std::ptrdiff_t;
		using size_type = std::size_t;
		using reverse_iterator = std::reverse_iterator<iterator>;
		using const_reverse_iterator = std::reverse_iterator<const_iterator>;

		[[ nodiscard ]]
		iterator begin() noexcept {
			return iterator{ getMinNode(root.get()), this };
		}

		[[ nodiscard ]]
		iterator end() noexcept {
			return iterator{ nullptr, this };
		}

		[[ nodiscard ]]
		const_iterator begin() const noexcept {
			return const_iterator{ getMinNode(root.get()), this };
		}

		[[ nodiscard ]]
		const_iterator end() const noexcept {
			return const_iterator{ nullptr, this };
		}

		[[ nodiscard ]]
		iterator find(const Key& key) noexcept (NoThrowComparable<CompareFn, Key>) {
			Node* result = search(key);
			if (result == nullptr)
				return end();

			return iterator{ result, this };
		}

		[[ nodiscard ]]
		const_iterator find(const Key& key) const noexcept (NoThrowComparable<CompareFn, Key>) {
			const Node* result = search(key);
			if (result == nullptr)
				return end();

			return iterator{ result, this };
		}

		template <typename K, typename V>
			requires std::same_as<std::decay_t<K>, Key>&& std::same_as<std::decay_t<V>, Val>
		bool insert(K&& key, V&& value) {
			if (root == nullptr) {
				root = std::make_unique<Node>(std::make_pair(std::forward<K>(key), std::forward<V>(value)), nullptr, nullptr, nullptr, Color::Red);
				return true;
			}

			Stack visited{};
			Node* current = root.get();

			bool isBefore = false, isAfter = false;
			while (current != nullptr) {
				isBefore = compare(key, current->keyValue.first);
				if (isBefore) {
					visited.push(current);
					current = current->left ? current->left.get() : nullptr;
					continue;
				}

				isAfter = compare(current->keyValue.first, key);
				if (isAfter) {
					visited.push(current);
					current = current->right ? current->right.get() : nullptr;
				}
				else {
					current = nullptr;
				}
			}

			const bool success = isBefore || isAfter;
			if (success) {
				Node* parent = visited.top();
				NodePtr& child = isBefore ? parent->left : parent->right;
				child = std::make_unique<Node>(std::make_pair(std::forward<K>(key), std::forward<V>(value)), nullptr, nullptr, parent, Color::Red);
				rebalance(visited);
			}

			return success;
		}

		bool erase(const Key& key) {
			Stack visited{};
			Node* current = root.get();
			bool isDeleted = false;
			while (current != nullptr && !isDeleted) {
				bool isBefore = compare(key, current->keyValue.first);
				if (isBefore) {
					if (!isRed(current->left) && current->left->left && !isRed(current->left->left))
						current = moveRedLeft(current);
					visited.push(current);
					current = current->left != nullptr ? current->left.get() : nullptr;
					continue;
				}
				else {
					if (isRed(current->left)) {
						current = rotateRight(current);
						isBefore = compare(key, current->keyValue.first);
					}

					bool isAfter = compare(current->keyValue.first, key);
					if (!isBefore && !isAfter && current->right == nullptr) {
						Node* parent = current->parent;
						NodePtr& child = parent->left && parent->left.get() == current ? parent->left : parent->right;
						child.reset();
						current = nullptr;
						isDeleted = true;
						continue;
					}
					else if (!isRed(current->right) && current->right->left && !isRed(current->right->left)) {
						current = moveRedRight(current);
						isBefore = compare(key, current->keyValue.first);
						isAfter = compare(current->keyValue.first, key);
					} 

					if (!isBefore && !isAfter) {
						Node* minNode = getMinNode(current->right.get());
						std::swap(current->keyValue, minNode->keyValue);
						deleteMinNode(current->right.get());
						isDeleted = true;
					}

					visited.push(current);
					current = current->right != nullptr ? current->right.get() : nullptr;
				}
			}

			rebalance(visited);

			return isDeleted;
		}

		[[ nodiscard ]]
		bool isValidRedBlackTree() const {
			if (root == nullptr)
				return true;

			bool isValid = root->color == Color::Black;

			using StackWithHeight = std::stack<std::pair<Node*, int>, std::vector<std::pair<Node*, int>>>;
			StackWithHeight inorder{};

			Node* current = root.get();
			const int treeBlackHeight = getBlackHeight(current);
			int currentBlackHeight = 0;
			while (isValid && (current != nullptr || !inorder.empty())) {
				if (current == nullptr) {
					const auto [next, blackHeight] = inorder.top();
					current = next;
					currentBlackHeight = blackHeight;
					inorder.pop();

					if (current->left == nullptr && current->right == nullptr)
						isValid = currentBlackHeight == treeBlackHeight;

					isValid = isValid && !isRed(current->right);
					current = current->right ? current->right.get() : nullptr;
				}
				else {
					if (current->color == Color::Black)
						++currentBlackHeight;
					inorder.push(std::make_pair(current, currentBlackHeight));
					current = current->left ? current->left.get() : nullptr; 
				}
			}

			return isValid;
		}

	};

	template <typename Key, typename Val, typename CompareFn>
		requires std::default_initializable<Key>&& std::default_initializable<Val>&&
	std::strict_weak_order<CompareFn, const Key&, const Key&>
		template <bool Const>
	class RedBlackTree<Key, Val, CompareFn>::Iterator {
	public:

		using iterator_category = std::bidirectional_iterator_tag;
		using value_type = std::conditional_t<Const, const std::pair<Key, Val>, std::pair<Key, Val>>;
		using difference_type = std::ptrdiff_t;
		using pointer = value_type*;
		using reference = value_type&;
		using container = RedBlackTree<Key, Val, CompareFn>;
		using node_type = container::Node;

		Iterator(node_type* node, const container* map) noexcept
			: current{ node }, map{ map }
		{
			assert(map != nullptr);
		}

		Iterator& operator++() noexcept
		{
			assert(current != nullptr);

			if (current->right != nullptr) {
				current = map->getMinNode(current->right.get());
				return *this;
			}

			node_type* parent = current->parent;
			if (parent != nullptr && parent->left != nullptr && parent->left.get() == current) {
				current = parent;
				return *this;
			}

			while (parent != nullptr && parent->right != nullptr && parent->right.get() == current) {
				current = parent;
				parent = parent->parent;
			}

			current = (parent != nullptr && parent->left != nullptr && parent->left.get() == current) ? parent : nullptr;
			return *this;
		}

		Iterator& operator++(int) noexcept {
			Iterator temp{ *this };
			++(*this);
			return temp;
		}

		Iterator& operator--() noexcept {
			assert(current != nullptr);

			if (current->left != nullptr) {
				current = map->getMaxNode(current->left.get());
				return *this;
			}

			node_type* parent = current->parent;
			if (parent != nullptr && parent->right != nullptr && parent->right.get() == current) {
				current = parent;
				return *this;
			}

			while (parent != nullptr && parent->left != nullptr && parent->left.get() == current) {
				current = parent;
				parent = parent->parent;
			}

			current = (parent != nullptr && parent->right != nullptr && parent->right.get() == current) ? parent : nullptr;
			//current = parent;
			return *this;
		}

		Iterator& operator--(int) noexcept {
			Iterator temp{ *this };
			--(*this);
			return temp;
		}

		reference operator*() const noexcept
		{
			return current->keyValue;
		}

		pointer operator->() const noexcept
		{
			return std::addressof(current->keyValue);
		}

		bool operator==(const Iterator& rhs) const noexcept {
			return current == rhs.current;
		}

		bool operator!=(const Iterator& rhs) const noexcept {
			return !(*this == rhs);
		}

	private:

		node_type* current;
		const container* map;

	};
}