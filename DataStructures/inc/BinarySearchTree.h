#pragma once

#include <memory>
#include <utility>
#include <concepts>
#include <optional>
#include <iterator>
#include <functional>
#include <type_traits>
#include <cassert>

namespace Rw
{
	template <typename CompareFn, typename T>
	concept NoThrowComparable = requires (CompareFn compare, const T& arg1, const T& arg2) {
		{ compare(arg1, arg2) } noexcept;
	};

	template <typename T>
	concept NoThrowDefaultConstructible = requires {
		{ T{} } noexcept;
	};

	template <typename Key, typename Val, typename CompareFn = std::less<Key>>
		requires std::default_initializable<Key> && std::default_initializable<Val>
			&& std::strict_weak_order<CompareFn, const Key&, const Key&>
	class BinarySearchTree {
	private:

		struct Node;
		using NodePtr = std::unique_ptr<Node>;

		struct Node {
			std::pair<Key, Val> keyValue;
			NodePtr left;
			NodePtr right;
			Node* parent;
		};
		
		NodePtr root;
		Node* revBegin;
		CompareFn compare;

		[[ nodiscard ]]
		Node* search(const Key& key) const noexcept (NoThrowComparable<CompareFn, Key>) {
			Node* result{};
			if (root == nullptr)
				return result;

			Node* current{ root.get() };
			while (current != nullptr) {
				const bool isBefore = compare(key, current->keyValue.first);

				if (isBefore) {
					current = current->left ? current->left.get() : nullptr;
					continue;
				}

				const bool isAfter = compare(current->keyValue.first, key);

				if (isAfter) {
					current = current->right ? current->right.get() : nullptr;
				}
				else if (current != revBegin) {
					result = current;
					current = nullptr;
				}
			}

			return result;
		}

		[[ nodiscard ]]
		Node* getMinNode(Node* subtree) const noexcept {
			Node* minNode = subtree;
			while (minNode->left != nullptr) {
				minNode = minNode->left.get();
			}
			return minNode;
		}

		[[ nodiscard ]]
		Node* getMaxNode(Node* subtree) const noexcept {
			Node* maxNode = subtree;
			while (maxNode->right != nullptr && maxNode->right.get() != revBegin) {
				maxNode = maxNode->right.get();
			}
			return maxNode;
		}

	public:

		template <bool Const = false>
		class Iterator;

		using value_type = std::pair<Key, Val>;
		using pointer = value_type*;
		using reference = value_type&;
		using const_reference = const value_type&;
		using iterator = Iterator<>;
		using const_iterator = Iterator<true>;
		using difference_type = std::ptrdiff_t;
		using size_type = std::size_t;
		using reverse_iterator = std::reverse_iterator<iterator>;
		using const_reverse_iterator = std::reverse_iterator<const_iterator>;

		BinarySearchTree() :
			root{}, revBegin{}
		{
		}

		template <typename K, typename V>
			requires std::same_as<std::decay_t<K>, Key> && std::same_as<std::decay_t<V>, Val>
		bool insert(K&& key, V&& value) {
			if (root == nullptr) {
				root = std::make_unique<Node>(
					std::make_pair(std::forward<K>(key), std::forward<V>(value)), 
					nullptr, 
					std::make_unique<Node>(std::make_pair(Key{}, Val{}), nullptr, nullptr, root.get()), // reverse begin sentinel
					nullptr
				);
				revBegin = root->right.get();
				revBegin->parent = root.get();
				return true;
			}

			Node* current = root.get();
			while (current != nullptr) {
				const bool isBefore = compare(key, current->keyValue.first);
				if (isBefore) {
					if (current->left == nullptr) {
						current->left = std::make_unique<Node>(std::make_pair(std::forward<K>(key), std::forward<V>(value)), nullptr, nullptr, current);
						return true;
					}
						
					current = current->left.get();
					continue;
				} 

				const bool isAfter = compare(current->keyValue.first, key);
				if (isAfter) {
					if (current->right == nullptr) {
						current->right = std::make_unique<Node>(std::make_pair(std::forward<K>(key), std::forward<V>(value)), nullptr, nullptr, current);
						return true;
					}

					if (current->right != nullptr && current->right.get() == revBegin) {
						current->right = std::make_unique<Node>(std::make_pair(std::forward<K>(key), std::forward<V>(value)), nullptr, std::move(current->right), current);
						revBegin->parent = current->right.get();
						return true;
					}

					current = current->right.get();
				}
				else {		
					current = nullptr;	// stop here, key already present
				}
			}

			return false;
		}

		// Hibbard deletion algorithm
		bool erase(const Key& key) {
			Node* target = search(key);
			if (target == nullptr)
				return false;

			Node* parent = target->parent;
			if (target->left == nullptr && target->right == nullptr) {
				// 0 Children
				if (parent->right != nullptr && parent->right.get() == target) {
					parent->right.reset(nullptr);
				}
				else if (parent->left != nullptr && parent->left.get() == target) {
					parent->left.reset(nullptr);
				}
				return true;
			}
			else if (target->left == nullptr || target->right == nullptr) {
				// 1 Child
				NodePtr& child = target->left == nullptr ? target->right : target->left;
				if (parent->left != nullptr && parent->left.get() == target) {
					parent->left.reset(child.release());
				}
				else if (parent->right != nullptr && parent->right.get() == target) {
					parent->right.reset(child.release());
				}
				return true;
			}
			else {
				// 2 Children
				Node* successor = getMinNode(target->right.get());
				std::swap(target->keyValue, successor->keyValue);

				Node* successorParent = successor->parent;
				if (successorParent->left != nullptr && successorParent->left.get() == successor) {
					successorParent->left.reset(successor->right ? successor->right.release() : nullptr);
				}
				else if (successorParent->right != nullptr && successorParent->right.get() == successor) {
					successorParent->right.reset(successor->right ? successor->right.release() : nullptr);
				}
				return true;
			}

			return false;
		}

		[[ nodiscard ]]
		iterator find(const Key& key) const noexcept (NoThrowComparable<CompareFn, Key>) {
			Node* result = search(key);
			if (result == nullptr)
				return end();

			return iterator{ result, this };
		}

		iterator begin() const noexcept {
			return { getMinNode(root.get()), this };
		}

		iterator end() const noexcept {
			return { revBegin, this };
		}

		reverse_iterator rbegin() const noexcept
		{
			return reverse_iterator{ end() };
		}

		reverse_iterator rend() const noexcept
		{
			return reverse_iterator{ begin() };
		}
	};

	template <typename Key, typename Val, typename CompareFn>
		requires std::default_initializable<Key>&& std::default_initializable<Val> &&
			std::strict_weak_order<CompareFn, const Key&, const Key&>
		template <bool Const>
	class BinarySearchTree<Key, Val, CompareFn>::Iterator {
	public:

		using iterator_category = std::bidirectional_iterator_tag;
		using value_type = std::conditional_t<Const, const std::pair<Key, Val>, std::pair<Key, Val>>;
		using difference_type = std::ptrdiff_t;
		using pointer = value_type*;
		using reference = value_type&;
		using container = BinarySearchTree<Key, Val, CompareFn>;
		using node_type = container::Node;

		Iterator(node_type* node, const container* map) noexcept
			: current{ node }, map{ map }
		{
			assert(map != nullptr);
		}

		Iterator& operator++() noexcept
		{
			assert(current != nullptr);

			if (current->right != nullptr) {
				current = map->getMinNode(current->right.get());
				return *this;
			}

			node_type* parent = current->parent;
			if (parent != nullptr && parent->left != nullptr && parent->left.get() == current) {
				current = parent;
				return *this;
			}

			while (parent != nullptr && parent->right != nullptr && parent->right.get() == current) {
				current = parent;
				parent = parent->parent;
			}

			current = (parent != nullptr && parent->left != nullptr && parent->left.get() == current) ? parent : nullptr;
			return *this;
		}

		Iterator& operator++(int) noexcept {
			Iterator temp{ *this };
			++(*this);
			return temp;
		}

		Iterator& operator--() noexcept {
			assert(current != nullptr);

			if (current->left != nullptr) {
				current = map->getMaxNode(current->left.get());
				return *this;
			}

			node_type* parent = current->parent;
			if (parent != nullptr && parent->right != nullptr && parent->right.get() == current) {
				current = parent;
				return *this;
			}

			while (parent != nullptr && parent->left != nullptr && parent->left.get() == current) {
				current = parent;
				parent = parent->parent;
			}

			current = (parent != nullptr && parent->right != nullptr && parent->right.get() == current) ? parent : nullptr;
			//current = parent;
			return *this;
		}

		Iterator& operator--(int) noexcept {
			Iterator temp{ *this };
			--(*this);
			return temp;
		}

		reference operator*() const noexcept
		{
			return current->keyValue;
		}

		pointer operator->() const noexcept
		{
			return std::addressof(current->keyValue);
		}

		bool operator==(const Iterator& rhs) const noexcept {
			return current == rhs.current;
		}

		bool operator!=(const Iterator& rhs) const noexcept {
			return !(*this == rhs);
		}

	private:

		node_type* current;
		const container* map;

	};

}