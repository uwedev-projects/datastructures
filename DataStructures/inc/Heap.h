#pragma once 

#include <bit>
#include <vector>
#include <utility>
#include <concepts>
#include <algorithm>
#include <functional>
#include <type_traits>
#include <cassert>

namespace Rw
{

	template <typename CompareFn, typename T>
	concept NoThrowComparable = requires (CompareFn compare, const T& arg1, const T& arg2) {
		{ compare(arg1, arg2) } noexcept;
	};

	template <typename Key>
	concept NoThrowSwappable = requires (Key& a, Key& b) {
		{ std::swap(a, b) } noexcept;
	};

	template <typename Key, typename CompareFn = std::less<Key>>
		requires std::strict_weak_order<CompareFn, const Key&, const Key&> && std::swappable<Key>
	class BinaryHeap {
	private:

		std::vector<Key> tree;
		CompareFn compare;

		static constexpr std::integral auto getHeightAt(std::integral auto n) noexcept {
			assert(n != 0);
			return 8 * sizeof(n) - std::countl_zero(n) - 1;
		}

		static constexpr std::integral auto getParent(std::integral auto n) noexcept {
			return n >> 1;
		}

		static constexpr std::integral auto getFirstChild(std::integral auto n) noexcept {
			return n * 2;
		}

		void swim(std::size_t nodeIdx) noexcept(NoThrowComparable<CompareFn, Key> && NoThrowSwappable<Key>) {
			bool isValidHeap = false;
			for (std::size_t parentIdx = getParent(nodeIdx); nodeIdx > 1 && !isValidHeap; nodeIdx = parentIdx, parentIdx = getParent(nodeIdx)) {
				Key& node = tree[nodeIdx];
				Key& parent = tree[parentIdx];
				isValidHeap = compare(parent, node);

				if (!isValidHeap) {
					std::swap(node, parent);
				}
			}
		}

		void sink(std::size_t nodeIdx) noexcept(NoThrowComparable<CompareFn, Key>&& NoThrowSwappable<Key>) {
			bool isValidHeap = false;
			std::size_t swapIdx = getFirstChild(nodeIdx);
			while (swapIdx < tree.size() && !isValidHeap) {
				Key& node = tree[nodeIdx];
				Key* childToSwap = &tree[swapIdx];

				if (swapIdx + 1 < tree.size()) {
					Key& secondChild = tree[swapIdx + 1];
					const bool isFirstBeforeSecond = compare(*childToSwap, secondChild);
					childToSwap = isFirstBeforeSecond ? childToSwap : &secondChild;
					swapIdx = isFirstBeforeSecond ? swapIdx : swapIdx + 1;
				}

				isValidHeap = compare(node, *childToSwap);
				if (!isValidHeap) {
					std::swap(node, *childToSwap);
				}

				nodeIdx = swapIdx;
				swapIdx = getFirstChild(nodeIdx);
			}
		}

	public:

		BinaryHeap(std::initializer_list<Key> values)
			: tree{}, compare{}
		{
			tree.reserve(values.size() + 1);
			tree.emplace_back();

			for (const Key& value : values) {
				push(value);
			}
		}

		template <typename K>
			requires std::same_as<std::decay_t<K>, Key>
		void push(K&& value) {
			tree.emplace_back(std::forward<K>(value));
			swim(tree.size() - 1);
		}

		[[ nodiscard ]]
		Key pop() {
			assert(tree.size() > 1);

			const Key root{ std::move(tree[1]) };
			if (tree.size() == 2) [[unlikely]] {
				tree.pop_back();
				return root;
			}

			std::swap(tree[1], tree.back());
			tree.pop_back();
			sink(1);

			return root;
		}

		std::enable_if_t<std::copy_constructible<Key>, Key>
		[[ nodiscard ]]
		peek() {
			assert(tree.size() > 1);
			return tree[1];
		}

		bool empty() const noexcept {
			return tree.size() <= 1;
		}

		std::size_t size() const noexcept {
			assert(tree.size() > 0);
			return tree.size() - 1;
		}

	};

}